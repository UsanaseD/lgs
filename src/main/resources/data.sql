insert 
    into
        users
        (active, create_date_time, email, last_name,first_name, location_id, location_type, password, roles, update_date_time, user_name, id)
    values
        (true, current_timestamp, 'admin@lgs.rw', 'admin', 'admin',1000, 'DISTRICT', '$2y$12$X3lgNeOiloJmUWFIXYMgPeULA5tykop6ffo8dmZG7LUMoGkRjrfF.', 'ROLE_ADMIN', current_timestamp, 'admin', 0)
    on conflict ("id") do nothing;