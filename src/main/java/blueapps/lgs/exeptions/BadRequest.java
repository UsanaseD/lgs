package blueapps.lgs.exeptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
// import sun.awt.CausedFocusEvent;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadRequest extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 8453310610710490279L;

    public BadRequest(String message) {
        super(message);
    }
    // public BadRequest(String message , Throwable cause ){super(message, cause);}
}