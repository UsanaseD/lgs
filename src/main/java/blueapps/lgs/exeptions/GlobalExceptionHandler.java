package blueapps.lgs.exeptions;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;

@ControllerAdvice
public class GlobalExceptionHandler extends Throwable {

    /**
     *
     */
    private static final long serialVersionUID = 7774955579196393779L;

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<?> resourceNotfoundException(ResourceNotFoundException ex) {
        HttpStatus notFound = HttpStatus.NOT_FOUND;
        ErrorDetails errorDetails = new ErrorDetails(ZonedDateTime.now(ZoneId.of("Z")), ex.getMessage(), ex,
                HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(errorDetails, notFound);
    }

    @ExceptionHandler(ResourceAlreadyExistsException.class)
    public ResponseEntity<?> ResourceAlreadyExistsExceptionException(ResourceAlreadyExistsException ex) {
        HttpStatus conflict = HttpStatus.CONFLICT;
        ErrorDetails errorDetails = new ErrorDetails(ZonedDateTime.now(ZoneId.of("Z")), ex.getMessage(), ex,
                HttpStatus.CONFLICT);
        return new ResponseEntity<>(errorDetails, conflict);
    }

    @ExceptionHandler(UserAlreadyExistsExceptionException.class)
    public ResponseEntity<?> userAlreadyExistsExceptionException(UserAlreadyExistsExceptionException ex) {
        HttpStatus conflict = HttpStatus.CONFLICT;
        ErrorDetails errorDetails = new ErrorDetails(ZonedDateTime.now(ZoneId.of("Z")), ex.getMessage(), ex,
                HttpStatus.CONFLICT);
        return new ResponseEntity<>(errorDetails, conflict);
    }

    @ExceptionHandler(HttpClientErrorException.BadRequest.class)
    public ResponseEntity<?> badRequestException(BadRequest ex) {
        HttpStatus badRequest = HttpStatus.BAD_REQUEST;
        ErrorDetails errorDetails = new ErrorDetails(ZonedDateTime.now(ZoneId.of("Z")), ex.getMessage(), ex,
                HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(errorDetails, badRequest);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> globalExceptionHandler(Exception ex) {
        HttpStatus serverError = HttpStatus.INTERNAL_SERVER_ERROR;
        ErrorDetails errorDetails = new ErrorDetails(ZonedDateTime.now(ZoneId.of("Z")), ex.getMessage(), ex,
                HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(errorDetails, serverError);
    }

    @ExceptionHandler(Created.class)
    public ResponseEntity<?> createdExceptionHandler(Exception ex) {
        HttpStatus serverResp = HttpStatus.CREATED;
        ErrorDetails errorDetails = new ErrorDetails(ZonedDateTime.now(ZoneId.of("Z")), ex.getMessage(), ex,
                HttpStatus.CREATED);

        return new ResponseEntity<>(errorDetails, serverResp);
    }
}
