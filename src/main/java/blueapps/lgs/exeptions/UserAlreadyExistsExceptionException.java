package blueapps.lgs.exeptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class UserAlreadyExistsExceptionException extends RuntimeException {
    private  static final long serialVersionUID = 1L;
    public UserAlreadyExistsExceptionException(String message){super(message);}
}