package blueapps.lgs.exeptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CREATED)
public class Created extends  RuntimeException{
    /**
     *
     */
    private static final long serialVersionUID = -8716322846418867935L;

    public Created (String Message){super(Message);}
}
