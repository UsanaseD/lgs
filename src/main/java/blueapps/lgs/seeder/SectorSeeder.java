package blueapps.lgs.seeder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import blueapps.lgs.repositories.DistrictRepo;
import blueapps.lgs.repositories.SectorRepo;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@Data
public class SectorSeeder {
    @Autowired
    SectorRepo sectorRepo;

    @Autowired
    DistrictRepo districtRepo;
    
    public void seedSectorTable(){
        // List<SectorModel> sectors = sectorRepo.findAll();
        // if(sectors.isEmpty()){
        //     SectorModel kicukiro = new SectorModel();
        //     kicukiro.setName("Kicukiro");
        //     kicukiro.setSurface((long) 1000);
        //     kicukiro.setDistrict(districtRepo.findByName("Kicukiro"));
        //     sectorRepo.save(kicukiro);
        //     log.info("{}", "Sector created by SectorSeeder");
        // }
        log.warn("No sector added by seeder");
    }
}
