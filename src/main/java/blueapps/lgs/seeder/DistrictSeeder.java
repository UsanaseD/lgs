package blueapps.lgs.seeder;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import blueapps.lgs.models.DistrictModel;
import blueapps.lgs.repositories.DistrictRepo;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@Data
public class DistrictSeeder {

    @Autowired
    DistrictRepo districtRepo;
    
    public void seedDistrictTable(){
        List<DistrictModel> districts = districtRepo.findAll();
        if(districts.isEmpty()){
            DistrictModel kicukiro = new DistrictModel();
            kicukiro.setName("Kicukiro");
            kicukiro.setProvince("Kigali");
            kicukiro.setSurface((long) 1000);
            districtRepo.save(kicukiro);

            log.info("{}", "Districts created by DistrictSeeder");
        }
        log.warn("No district added by seeder");
    }
}
