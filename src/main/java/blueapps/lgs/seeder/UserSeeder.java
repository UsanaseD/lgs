package blueapps.lgs.seeder;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import blueapps.lgs.models.User;
import blueapps.lgs.models.dto.UserDto;
import blueapps.lgs.repositories.UserRepository;
import blueapps.lgs.services.UserService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@Data
public class UserSeeder {
    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;
    
    public void seedUsersTable(){
        List<User> users = userRepository.findAll(PageRequest.of(0, Integer.MAX_VALUE)).getContent();
        if(users.isEmpty()){
            UserDto user1 = new UserDto("user", "one", "userOne", "user1@gmail.com", "password", "VILLAGE", 1000, "ROLE_USER", true);
            UserDto admin = new UserDto("admin", "", "admin", "admin@gmail.com", "password", "VILLAGE", 1000, "ROLE_ADMIN", true);

            userService.createUser(user1.mapToUser());
            userService.createUser(admin.mapToUser());
            log.info("{}", "User created by UserSeeder");
        }
        log.warn("No user added by seeder");
    }


}
