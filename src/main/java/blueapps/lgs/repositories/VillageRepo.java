package blueapps.lgs.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import blueapps.lgs.models.CellModel;
import blueapps.lgs.models.VillageModel;

@Repository
public interface VillageRepo extends PagingAndSortingRepository<VillageModel,Long> {
    List<VillageModel> findByCell_Id(@Param("cell")long cellId);
    List<VillageModel> findByCell(CellModel cell);
    @Query(value = "SELECT rcds from VillageModel rcds where rcds.cell.id = :id")
    List<VillageModel> showVillages(@Param("id") Integer id);
    @Query(value = "SELECT rcds from VillageModel rcds where rcds.cell.sector.id = :id")
    List<VillageModel> showVillagesParSector(@Param("id") Long id);    
    Optional<VillageModel> findByNameContainingIgnoreCaseAndCell(String name, CellModel cell);
}
