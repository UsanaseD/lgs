package blueapps.lgs.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import blueapps.lgs.models.Category;
import blueapps.lgs.models.Record;
import blueapps.lgs.models.VillageModel;

@Repository
public interface RecordRepository extends PagingAndSortingRepository<Record, Long> {
  List<Record> findByVillage(VillageModel village);

  List<Record> findByCategory(Category category);

  List<Record> findByCategoryAndVillage(Category category, VillageModel village);

  @Query(value = "SELECT rcd FROM Record rcd WHERE rcd.category.id = :id ")
  List<Record> recordsByCateg(@Param("id") Integer id);

  Optional<Record> findByNid(String nid);

  @Query("select cat from Record cat where cat.village.cell.sector.id = :id")
  List<Record> RecordPerSector(@Param("id") Integer id);
}
