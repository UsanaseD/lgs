package blueapps.lgs.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import blueapps.lgs.models.DistrictModel;

@Repository
public interface DistrictRepo extends JpaRepository<DistrictModel, Long> {
    @Query("SELECT  dist FROM  DistrictModel dist WHERE dist.province LIKE %?1%")
    List<DistrictModel> search(String keyword);
    DistrictModel findByName(String name);
    List<DistrictModel> findByProvinceContainingIgnoreCase(String province);
}
