package blueapps.lgs.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import blueapps.lgs.models.Category;
import blueapps.lgs.models.ClusterModel;

@Repository
public interface CategoryRepository extends PagingAndSortingRepository<Category, Integer> {
  
    @Query(value = "SELECT ctgr FROM Category ctgr WHERE ctgr.cluster.user.id = :id ")
    static
    List<Category> owenedByCreater(@Param("id") Integer id) {
        // TODO Auto-generated method stub
        return null;
    }

    // @Query(value = "SELECT count (ctgr) FROM Category ctgr WHERE ctgr.cluster.user.id = :id ")
    // Long CategoryByUser(@Param("id") Integer id);

    // @Query(value = "SELECT ctgr FROM Category ctgr WHERE ctgr.cluster.user.locationId = :id ")
    // List<Category> CategoryByLocation(@Param("id") Integer id);

    List<Category> findByCluster(ClusterModel cluster);
    
    // @Query("select e from Category e where e.cluster.user.id = :id and year(e.createDateTime) = year(current_date) and  month(e.createDateTime) = month(current_date) ")
    // List<Category> getAllOfCurrentMonth(@Param("id")Integer id);

    Optional<Category> findByName(String name);
}