package blueapps.lgs.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import blueapps.lgs.models.History;
import blueapps.lgs.models.User;

@Repository
public interface HistoryRepository extends PagingAndSortingRepository<History, Integer> {
    Page<History> findByUser(User user, Pageable pageable);
}
