package blueapps.lgs.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import blueapps.lgs.models.User;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Integer> {

  public Optional<User> findByEmail(String email);
  public Optional<User> findByUserName(String userName);
  public Optional<User> findByLocationTypeAndLocationIdAndRolesContaining(User.ELOCATIONTYPE locationType, Integer locationId, String roles);
  public List<User> findByLocationTypeAndLocationId(User.ELOCATIONTYPE locationType, Integer locationId);
    
 }