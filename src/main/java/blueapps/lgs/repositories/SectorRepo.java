package blueapps.lgs.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import blueapps.lgs.models.DistrictModel;
import blueapps.lgs.models.SectorModel;

@Repository
public interface SectorRepo extends PagingAndSortingRepository<SectorModel,Long> {
    List<SectorModel> findByDistrict_Id(@Param("district")long districtId);
    List<SectorModel> findByDistrict(DistrictModel district);
    Optional<SectorModel> findByNameContainingIgnoreCaseAndDistrict(String name, DistrictModel district);
}
