package blueapps.lgs.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import blueapps.lgs.models.CellModel;
import blueapps.lgs.models.SectorModel;

@Repository
public interface CellRepo extends PagingAndSortingRepository<CellModel,Long> {
    List<CellModel> findBySector_Id(@Param("sector")long sectId);
    List<CellModel> findBySector(SectorModel sector);
    Optional<CellModel> findByNameContainingIgnoreCaseAndSector(String name, SectorModel sector);
}
