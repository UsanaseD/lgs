
package blueapps.lgs.repositories;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import blueapps.lgs.models.ClusterModel;

@Repository
public interface ClusterRepo extends PagingAndSortingRepository<ClusterModel, Integer> {
    Optional<ClusterModel> findByEmail(String email);
}
