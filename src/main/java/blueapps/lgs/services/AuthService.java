package blueapps.lgs.services;

import blueapps.lgs.models.User;
import blueapps.lgs.models.Response.LocationResponse;

public interface AuthService {

    User getLoggedInUser();

    LocationResponse getLocation();

}
