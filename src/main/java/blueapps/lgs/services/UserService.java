package blueapps.lgs.services;

import java.util.List;

import blueapps.lgs.exeptions.ResourceNotFoundException;
import blueapps.lgs.models.User;

public interface UserService {
    User createUser(User user);
    User updateUser(User user, Integer id) throws ResourceNotFoundException;
    User getUserById(Integer id) throws ResourceNotFoundException;
    User getUserByUserName(String userName) throws ResourceNotFoundException;
    User getUserByEmail(String email) throws ResourceNotFoundException;
    List<User> getAllUsers(int page, int limit);
    List<User> getUsersByLocation(String locationType, Integer locationId);
}
