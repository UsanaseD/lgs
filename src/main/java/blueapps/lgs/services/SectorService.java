package blueapps.lgs.services;

import java.util.List;

import blueapps.lgs.models.dto.SectorDto;

public interface SectorService {
    SectorDto createSector(SectorDto sectorDto, Long districtId);
    SectorDto updateSector(SectorDto sectorDto, Long id, Long districtId);
    List<SectorDto> getSectors();
    SectorDto getSectorById(Long id);
    List<SectorDto> getSectorsByDistrict(Long districtId);
}
