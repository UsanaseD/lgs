package blueapps.lgs.services.servicesImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import blueapps.lgs.exeptions.ResourceNotFoundException;
import blueapps.lgs.models.Category;
import blueapps.lgs.models.ClusterModel;
import blueapps.lgs.models.Response.ClusterResponse;
import blueapps.lgs.models.dto.ClusterDto;
import blueapps.lgs.repositories.CategoryRepository;
import blueapps.lgs.repositories.ClusterRepo;
import blueapps.lgs.services.AuthService;
import blueapps.lgs.services.CellService;
import blueapps.lgs.services.ClusterService;
import blueapps.lgs.services.DistrictService;
import blueapps.lgs.services.RecordService;
import blueapps.lgs.services.SectorService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ClusterServiceImpl implements ClusterService {
    @Autowired
    ClusterRepo clusterRepo;

    @Autowired
    AuthService authService;

    @Autowired
    RecordService recordService;

    @Autowired
    CellService cellService;

    @Autowired
    SectorService sectorService;

    @Autowired
    DistrictService districtService;

    @Autowired
    CategoryRepository categoryRepository;

    public ClusterDto createCluster(ClusterDto clusterDto) {
        int records = 0;
        clusterDto.setStatus("active");
        clusterDto.setDeleted(false);
        ClusterModel cluster = clusterRepo.save(clusterDto.mapToEntity());

        ClusterDto createdDto = cluster.mapToDto();
        // createdDto.setNumberOfCategories(categoryRepository.findByCluster(cluster).size());
        // records = this.countRecordsByCluster(cluster);
        // createdDto.setRecords(records);
        return createdDto;
    }

    public ResponseEntity<?> getErrors(BindingResult bindingResult) {
        return getResponseEntity(bindingResult);
    }

    static ResponseEntity<?> getResponseEntity(BindingResult bindingResult) {
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        Stream<String> stringStream = fieldErrors.stream().map(fieldError -> fieldError.getDefaultMessage());
        String error = stringStream.findFirst().get();
        return ResponseEntity.badRequest().body(error);
    }

    public Optional<ClusterModel> emailExst(String email) {
        return clusterRepo.findByEmail(email);
    }

    public ClusterDto getClusterById(Integer id) {
        ClusterModel cluster = clusterRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Cluster not found"));
        ClusterDto clusterDto = cluster.mapToDto();

        // clusterDto.setNumberOfCategories(categoryRepository.findByCluster(cluster).size());

        // int records = this.countRecordsByCluster(cluster);
        // clusterDto.setRecords(records);
        // List<Record> records = new ArrayList<Record>();
        // ClusterResponse res = cluster.mapToResponse();
        // List<Category> categories = cluster.getCategories();
        // categories.forEach(ca -> {
        // records.addAll(recordService.getRecordByCategory(ca));
        // });
        // res.setCategories(cluster.getCategories().size());
        // res.setRecords(records.size());
        // log.info("{}", res);
        return clusterDto;
    }

    public List<ClusterDto> getAllClusters(int page, int limit) {
        List<ClusterDto> clusterDtos = new ArrayList<>();
        if (page > 0)
            page = page - 1;
        Page<ClusterModel> clustersPage = clusterRepo.findAll(PageRequest.of(page, limit));
        List<ClusterModel> clusters = clustersPage.getContent();
        clusters.forEach(c -> {
            ClusterDto clusterDto = c.mapToDto();

            // clusterDto.setNumberOfCategories(categoryRepository.findByCluster(c).size());

            // int records = this.countRecordsByCluster(c);
            // clusterDto.setRecords(records);

            // clusterDto.setRecords(records);
            clusterDtos.add(clusterDto);
        });
        return clusterDtos;
    }

    public ClusterDto updateCluster(ClusterDto clusterDto, Integer id) {
        ClusterModel cluster = clusterRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Cluster not found"));
        cluster.setName(clusterDto.getName());
        cluster.setPhone(clusterDto.getPhone());
        cluster.setEmail(clusterDto.getEmail());
        cluster.setWebsite(clusterDto.getWebsite());
        cluster.setDescription(clusterDto.getDescription());

        ClusterModel updatedCluster = clusterRepo.save(cluster);
        ClusterDto updatedClusterDto = updatedCluster.mapToDto();
        // updatedClusterDto.setRecords(this.countRecordsByCluster(cluster));
        // updatedClusterDto.setNumberOfCategories(categoryRepository.findByCluster(cluster).size());

        return updatedClusterDto;
    }

    public int countRecordsByCluster(ClusterModel cluster) {
        List<Category> categories = cluster.getCategories();
        if (categories == null) {
            categories = new ArrayList<>();
        }
        int records = 0;
        for (Category category : categories) {
            records = records + recordService.getRecordByCategory(category).size();
        }
        return records;
    }

    @Override
    public ClusterResponse mapDtoToResponse(ClusterDto clusterDto) {
        ClusterResponse clusterResponse = new ClusterResponse();
        ClusterModel cluster = clusterRepo.findById(clusterDto.getId()).orElseThrow(() -> new ResourceNotFoundException("Cluster not found ...."));
        BeanUtils.copyProperties(clusterDto, clusterResponse);        
        clusterResponse.setRecords(this.countRecordsByCluster(cluster));
        clusterResponse.setNumberOfCategories(categoryRepository.findByCluster(cluster).size());
        return clusterResponse;
    }

}
