package blueapps.lgs.services.servicesImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import blueapps.lgs.exeptions.ResourceAlreadyExistsException;
import blueapps.lgs.exeptions.ResourceNotFoundException;
import blueapps.lgs.models.CellModel;
import blueapps.lgs.models.SectorModel;
import blueapps.lgs.models.dto.CellDto;
import blueapps.lgs.repositories.CellRepo;
import blueapps.lgs.repositories.SectorRepo;
import blueapps.lgs.services.CellService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CellServiceImpl implements CellService {
    @Autowired
    SectorRepo sectorRepo;

    @Autowired
    CellRepo cellRepo;

    @Override
    public CellDto createCell(CellDto cellDto, Long sectorId) {
        CellModel cellToCreate = cellDto.mapToEntity();
        SectorModel sector = sectorRepo.findById(sectorId).orElseThrow(() -> new ResourceNotFoundException("Sector not found"));
        
        Optional<CellModel> checkIfCellExist = cellRepo.findByNameContainingIgnoreCaseAndSector(cellToCreate.getName(), sector);
        if(checkIfCellExist.isPresent()){
            throw new ResourceAlreadyExistsException("Cell already recorded...");
        }
        
        cellToCreate.setSector(sector);

        CellModel createdCell = cellRepo.save(cellToCreate);

        return createdCell.mapToDto();
    }

    @Override
    public List<CellDto> getCells() {
        List<CellDto> returnValue = new ArrayList<>();
        List<CellModel> cells = cellRepo.findAll(PageRequest.of(0, Integer.MAX_VALUE)).getContent();
        
        for(CellModel cell: cells){
            CellDto cellDto = cell.mapToDto();
            returnValue.add(cellDto);
        }

        return returnValue;
    }

    @Override
    public CellDto getCellById(Long id) {
        CellModel cell = cellRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Cell not found.."));
        return cell.mapToDto();
    }

    @Override
    public List<CellDto> getCellsBySector(Long sectorId) {
        List<CellDto> cellDtos = new ArrayList<>();
        SectorModel sector = sectorRepo.findById(sectorId).orElseThrow(() -> new ResourceNotFoundException("Sector not found ..."));
        List<CellModel> cells = cellRepo.findBySector(sector);

        for(CellModel cell : cells){
            CellDto cellDto = cell.mapToDto();
            cellDtos.add(cellDto);
        }

        return cellDtos;
    }

    @Override
    public CellDto updateCell(CellDto cellDto, Long id, Long sectorId) {
        CellModel cell = cellRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Cell not found"));
        SectorModel sector = sectorRepo.findById(sectorId).orElseThrow(() -> new ResourceNotFoundException("Sector not found"));
        
        cell.setName(cellDto.getName());
        cell.setSurface(cellDto.getSurface());
        cell.setSector(sector); 
        
        CellModel updatedCell = cellRepo.save(cell);
        
        return updatedCell.mapToDto();
    }
    
}
