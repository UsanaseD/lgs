package blueapps.lgs.services.servicesImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import blueapps.lgs.exeptions.ResourceAlreadyExistsException;
import blueapps.lgs.exeptions.ResourceNotFoundException;
import blueapps.lgs.models.CellModel;
import blueapps.lgs.models.VillageModel;
import blueapps.lgs.models.dto.VillageDto;
import blueapps.lgs.repositories.CellRepo;
import blueapps.lgs.repositories.VillageRepo;
import blueapps.lgs.services.VillageService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class VillageServiceImpl implements VillageService {
    @Autowired
    VillageRepo villageRepo;

    @Autowired
    CellRepo cellRepo;

    @Override
    public VillageDto createVillage(VillageDto villageDto, Long cellId) {
        CellModel cell = cellRepo.findById(cellId).orElseThrow(() -> new ResourceNotFoundException("Village not found..."));
        Optional<VillageModel> checkVillage = villageRepo.findByNameContainingIgnoreCaseAndCell(villageDto.getName(), cell);
        if(checkVillage.isPresent()){
            throw new ResourceAlreadyExistsException("Village is already registered");
        }
        VillageModel village = villageDto.mapToEntity();
        village.setCell(cell);
        VillageModel createdVillage = villageRepo.save(village);
        return createdVillage.mapToDto();
    }

    @Override
    public List<VillageDto> getVillages() {
        List<VillageDto> villageDtos = new ArrayList<>();
        List<VillageModel> villages = villageRepo.findAll(PageRequest.of(0, Integer.MAX_VALUE)).getContent();
        for (VillageModel villageModel : villages) {
            villageDtos.add(villageModel.mapToDto());
        }
        return villageDtos;
    }

    @Override
    public VillageDto getVillageById(Long villageId) {
        VillageModel village = villageRepo.findById(villageId).orElseThrow(() -> new ResourceNotFoundException("Village not found..."));
        return village.mapToDto();
    }

    @Override
    public VillageDto updateVillage(VillageDto villageDto, Long villageId, Long cellId) {
        CellModel cell = cellRepo.findById(cellId).orElseThrow(() -> new ResourceNotFoundException("Cell not found"));
        VillageModel village = villageRepo.findById(villageId).orElseThrow(() -> new ResourceNotFoundException("Village not found"));
        village.setName(villageDto.getName());
        village.setSurface(villageDto.getSurface());
        village.setAmasibo(villageDto.getAmasibo());
        village.setCell(cell);

        VillageModel updatedVillage = villageRepo.save(village);
        return updatedVillage.mapToDto();
    }

    @Override
    public List<VillageDto> getVillagesByCell(Long cellId) {
        List<VillageDto> villageDtos = new ArrayList<>();
        CellModel cell = cellRepo.findById(cellId).orElseThrow(() -> new ResourceNotFoundException("Cell not found"));
        List<VillageModel> villageModels = villageRepo.findByCell(cell);

        for (VillageModel villageModel : villageModels) {
            villageDtos.add(villageModel.mapToDto());
        }

        return villageDtos;
    }
    
}
