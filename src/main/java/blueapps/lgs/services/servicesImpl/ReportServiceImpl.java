package blueapps.lgs.services.servicesImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import blueapps.lgs.models.Category;
import blueapps.lgs.models.Record;
import blueapps.lgs.models.Response.ReportResponse;
import blueapps.lgs.models.dto.RecordDto;
import blueapps.lgs.repositories.CategoryRepository;
import blueapps.lgs.repositories.RecordRepository;
import blueapps.lgs.services.CategoryService;
import blueapps.lgs.services.CellService;
import blueapps.lgs.services.ClusterService;
import blueapps.lgs.services.RecordService;
import blueapps.lgs.services.ReportService;
import blueapps.lgs.services.VillageService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ReportServiceImpl implements ReportService {

    @Autowired
    CategoryService categoryService;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    RecordRepository recordRepository;

    @Autowired
    RecordService recordService;

    @Autowired
    CellService cellService;

    @Autowired
    VillageService villageService;

    @Autowired
    ClusterService clusterService;

    

    @Override
    public List<ReportResponse> getReports() {
        List<Record> records = recordRepository.findAll(PageRequest.of(0, Integer.MAX_VALUE)).getContent();
        log.info("{}", records);
        return this.getReportsByRecord(records);
    }

    public List<ReportResponse> getReportsByRecord(List<Record> records){
        List<Category> categories = categoryRepository.findAll(PageRequest.of(0, Integer.MAX_VALUE)).getContent();
        List<ReportResponse> reports = new ArrayList<>();
        categories.forEach(category -> {
            ReportResponse report = new ReportResponse();
            report.setName(category.getName());
            report.setSize(records.stream().filter(r -> r.getCategory().getId() == category.getId()).collect(Collectors.toList()).size());
            reports.add(report);
        });
        return reports;
    }

    @Override
    public List<ReportResponse> getReportByLocation(String locationType, Integer locationId) {
        List<RecordDto> recordDtos = recordService.getRecordByLocation(locationType, locationId);
        List<Record> records = new ArrayList<>();
        log.info("{}", recordDtos);
        for (RecordDto recordDto : recordDtos) {
            records.add(recordDto.mapToRecord());
        }
        log.info("{}", records);
        return this.getReportsByRecord(records);
    }

}
