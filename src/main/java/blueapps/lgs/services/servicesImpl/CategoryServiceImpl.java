package blueapps.lgs.services.servicesImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import blueapps.lgs.exeptions.ResourceNotFoundException;
import blueapps.lgs.models.Category;
import blueapps.lgs.models.ClusterModel;
import blueapps.lgs.models.Response.CategoryResponse;
import blueapps.lgs.models.dto.CategoryDto;
import blueapps.lgs.repositories.CategoryRepository;
import blueapps.lgs.repositories.ClusterRepo;
import blueapps.lgs.services.AuthService;
import blueapps.lgs.services.CategoryService;
import blueapps.lgs.services.ClusterService;
import blueapps.lgs.services.RecordService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    ClusterRepo clusterRepository;

    @Autowired
    ClusterService clusterService;

    @Autowired
    RecordService recordService;

    @Autowired
    AuthService authService;

    @Override
    public List<CategoryDto> getAllCategories(int page, int limit) {
        List<CategoryDto> categoryDtos = new ArrayList<>();
        if(page>0) page = page-1;
        Page<Category> categoriesPage = categoryRepository.findAll(PageRequest.of(page, limit));
        List<Category> categories= categoriesPage.getContent();

        for (Category category : categories) {
            CategoryDto categoryDto = category.mapToDto();
            // categoryDto.setRecords(recordService.getRecordByCategory(category).size());
            categoryDtos.add(categoryDto);
        }

        return categoryDtos;
    }

    @Override
    public CategoryDto getCategoryById(Integer id) {
        CategoryDto categoryDto = categoryRepository
        .findById(id)
        .orElseThrow(() -> new ResourceNotFoundException("Category not found")).mapToDto();
        // categoryDto.setRecords(recordService.getRecordByCategory(categoryRepository.findById(categoryDto.getId()).get()).size());
        return categoryDto;
    }
    
    @Override
    public List<CategoryDto> getCategoriesParOwner(Integer id) {
        List<CategoryDto> categoryDtos = new ArrayList<>();
        List<Category> categ = CategoryRepository.owenedByCreater(id);
        log.info("{}", categ);
        for (Category category : categ) {
            CategoryDto categoryDto = category.mapToDto();
            categoryDtos.add(categoryDto);
        }
        return categoryDtos;    
    }


    @Override
    public CategoryDto createCategory(CategoryDto categoryDto, Integer clusterId) {
        ClusterModel cluster = clusterRepository.findById(clusterId).orElseThrow(() -> new ResourceNotFoundException("Cluster not found"));
        
        Category category = categoryDto.mapToEntity();
        category.setDeleted(false);
        category.setStatus("active");
        category.setCluster(cluster);
        
        Category createdCategory = categoryRepository.save(category);

        CategoryDto createdDto = createdCategory.mapToDto();
        // createdDto.setRecords(recordService.getRecordByCategory(categoryRepository.findById(createdDto.getId()).get()).size());
        return createdDto;
    }

    @Override
    public CategoryDto updateCategory(CategoryDto categorydDto, Integer clusterId, Integer id) {
        ClusterModel cluster = clusterRepository.findById(clusterId).orElseThrow(() -> new ResourceNotFoundException("Cluster not found"));
        
        Category category = categoryRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Category not found"));
        category.setName(categorydDto.getName());
        category.setDescription(categorydDto.getDescription());
        category.setCluster(cluster);
        category.setStatus("active");
        category.setDeleted(false);
        
        Category createdCategory = categoryRepository.save(category);

        CategoryDto createdDto = createdCategory.mapToDto();
        // createdDto.setRecords(recordService.getRecordByCategory(categoryRepository.findById(createdDto.getId()).get()).size());
        return createdDto;
    }

    @Override
    public CategoryResponse mapDtoToResponse(CategoryDto categoryDto) {
        CategoryResponse categoryResponse = new CategoryResponse();
        BeanUtils.copyProperties(categoryDto, categoryResponse);
        categoryResponse.setCluster(clusterService.mapDtoToResponse(categoryDto.getCluster()));
        categoryResponse.setRecords(recordService.getRecordByCategory(categoryRepository.findById(categoryDto.getId()).get()).size());
        return categoryResponse;
    }
}
