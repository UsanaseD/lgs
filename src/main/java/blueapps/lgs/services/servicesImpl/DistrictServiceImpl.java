package blueapps.lgs.services.servicesImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import blueapps.lgs.exeptions.ResourceNotFoundException;
import blueapps.lgs.models.DistrictModel;
import blueapps.lgs.models.dto.DistrictDto;
import blueapps.lgs.repositories.DistrictRepo;
import blueapps.lgs.services.DistrictService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DistrictServiceImpl implements DistrictService {

    @Autowired
    DistrictRepo districtRepo;

    @Override
    public DistrictDto createDistrict(DistrictDto districtDto) {
        DistrictModel district = districtDto.mapToEntity();
        DistrictModel storedDistrict = districtRepo.save(district);

        DistrictDto returnValue = storedDistrict.mapToDto();
        return returnValue;
    }

    @Override
    public List<DistrictDto> getDistricts() {
        List<DistrictDto> returnValue = new ArrayList<>();

        Page<DistrictModel> districtsPage = districtRepo.findAll(PageRequest.of(0, Integer.MAX_VALUE));
        List<DistrictModel> districtModels = districtsPage.getContent();

        log.info("{}", districtModels);

        for (DistrictModel district : districtModels) {
            returnValue.add(district.mapToDto());
        }

        log.info("{}", returnValue);
        return returnValue;
    }

    @Override
    public DistrictDto getDistrict(Long id) {
        DistrictModel district = districtRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("District not found"));
        return district.mapToDto();
    }

    @Override
    public DistrictDto updateDistrict(DistrictDto districtDto, Long id) {
        DistrictModel district = districtRepo.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("District not found"));

        district.setName(districtDto.getName());
        district.setProvince(districtDto.getProvince());
        district.setSurface(districtDto.getSurface());

        DistrictModel updatedDistrict = districtRepo.save(district);

        return updatedDistrict.mapToDto();
    }

    @Override
    public List<DistrictDto> getDistrictsByProvince(String province) {
        List<DistrictDto> returnValue = new ArrayList<>();

        List<DistrictModel> districtModels = districtRepo.findByProvinceContainingIgnoreCase(province);

        log.info("{}", districtModels);

        for (DistrictModel district : districtModels) {
            returnValue.add(district.mapToDto());
        }

        log.info("{}", returnValue);
        return returnValue;
    }
}
