package blueapps.lgs.services.servicesImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import blueapps.lgs.config.SecurityConfiguration;
import blueapps.lgs.exeptions.ResourceNotFoundException;
import blueapps.lgs.exeptions.UserAlreadyExistsExceptionException;
import blueapps.lgs.models.CellModel;
import blueapps.lgs.models.DistrictModel;
import blueapps.lgs.models.SectorModel;
import blueapps.lgs.models.User;
import blueapps.lgs.models.VillageModel;
import blueapps.lgs.repositories.CellRepo;
import blueapps.lgs.repositories.DistrictRepo;
import blueapps.lgs.repositories.SectorRepo;
import blueapps.lgs.repositories.UserRepository;
import blueapps.lgs.repositories.VillageRepo;
import blueapps.lgs.services.UserService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    SecurityConfiguration security;
    @Autowired
    DistrictRepo districtRepository;
    @Autowired
    SectorRepo sectorRepository;
    @Autowired
    CellRepo cellRepository;
    @Autowired
    VillageRepo villageRepository;

    @Override
    public User createUser(User user) {
        log.info("{}");
        Optional<User> rolesCheck = userRepository.findByLocationTypeAndLocationIdAndRolesContaining(
                user.getLocationType(), user.getLocationId(), user.getRoles());
        if (rolesCheck.isPresent()) {
            throw new UserAlreadyExistsExceptionException("Already have a user with role ::" + user.getRoles() + ":: in this " + user.getLocationType().toString().toLowerCase());
        }
        Long id = (long) user.getLocationId();
        String locationType = user.getLocationType().toString();
        if (locationType.matches("DISTRICT")) {
            Optional<DistrictModel> district = districtRepository.findById(id);
            if (!district.isPresent()) {
                throw new ResourceNotFoundException("District  with id ::" + id + ":: doesn't exists");
            }
        }else if (locationType.matches("SECTOR")) {
                Optional<SectorModel> sector = sectorRepository.findById(id);
                if (!sector.isPresent()) {
                    throw new ResourceNotFoundException("Sector  with id ::" + id + ":: doesn't exists");
                }
            } else if (locationType.matches("CELL")) {
                Optional<CellModel> cell = cellRepository.findById(id);
                if (!cell.isPresent()) {
                    throw new ResourceNotFoundException("Cell  with id ::" + id + ":: doesn't exists");
                }
            } else if (locationType.matches("VILLAGE")) {
                Optional<VillageModel> village = villageRepository.findById(id);
                if (!village.isPresent()) {
                    throw new ResourceNotFoundException("Village  with id ::" + id + ":: doesn't exists");
                }
            }
                user.setPassword(security.getPasswordEncoder().encode(user.getPassword()));
                return userRepository.save(user);

        }
    @Override
    public List<User> getAllUsers(int page, int limit) {
        if(page>0) page = page-1;
        Page<User> usersPage = userRepository.findAll(PageRequest.of(page, limit));
        List<User> users = usersPage.getContent();
        return users;
    }

    @Override
    public User getUserById(Integer id) throws ResourceNotFoundException {
        return userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User not found"));
    }

    @Override
    public User updateUser(User user, Integer id) throws ResourceNotFoundException {
        User userToUpdate = this.getUserById(id);
        user.setId(userToUpdate.getId());
        user.setCreateDateTime(userToUpdate.getCreateDateTime());
        user.setPassword(security.getPasswordEncoder().encode(user.getPassword()));

        return userRepository.save(user);
    }

    @Override
    public List<User> getUsersByLocation(String locationType, Integer locationId) {
        locationType = locationType.toUpperCase();
        return userRepository.findByLocationTypeAndLocationId(User.ELOCATIONTYPE.valueOf(locationType), locationId);
    }

    @Override
    public User getUserByUserName(String userName) throws ResourceNotFoundException {
        return userRepository.findByUserName(userName).orElseThrow(() -> new ResourceNotFoundException("User not found"));
    }

    @Override
    public User getUserByEmail(String email) throws ResourceNotFoundException {
        return userRepository.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException("User not found"));
    }

}
