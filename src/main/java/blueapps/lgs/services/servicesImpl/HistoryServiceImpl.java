package blueapps.lgs.services.servicesImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import blueapps.lgs.exeptions.ResourceNotFoundException;
import blueapps.lgs.models.History;
import blueapps.lgs.models.User;
import blueapps.lgs.models.dto.RecordDto;
import blueapps.lgs.repositories.HistoryRepository;
import blueapps.lgs.repositories.UserRepository;
import blueapps.lgs.services.AuthService;
import blueapps.lgs.services.HistoryService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HistoryServiceImpl implements HistoryService {

    @Autowired
    HistoryRepository historyRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthService authService;

    @Override
    public History storeHistory(RecordDto record, String action) {
        History history = new History(record.getId().intValue(), record.getFirstName() + " " + record.getLastName(),
                record.getNid(), action);
        history.setUser(authService.getLoggedInUser());
        return historyRepository.save(history);
    }

    @Override
    public List<History> getAllHistory(int page, int limit) {
        return historyRepository.findAll(PageRequest.of(0, Integer.MAX_VALUE)).getContent();
    }

    @Override
    public List<History> getHistoryByCreatorId(Long creatorId) {
        User user = userRepository.findById(creatorId.intValue())
                .orElseThrow(() -> new ResourceNotFoundException("User not found"));
        return historyRepository.findByUser(user, PageRequest.of(0, Integer.MAX_VALUE)).getContent();
    }

}
