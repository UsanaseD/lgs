package blueapps.lgs.services.servicesImpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import blueapps.lgs.exeptions.ResourceNotFoundException;
import blueapps.lgs.models.CellModel;
import blueapps.lgs.models.DistrictModel;
import blueapps.lgs.models.MyUserDetails;
import blueapps.lgs.models.SectorModel;
import blueapps.lgs.models.User;
import blueapps.lgs.models.VillageModel;
import blueapps.lgs.models.Response.LocationResponse;
import blueapps.lgs.repositories.CellRepo;
import blueapps.lgs.repositories.DistrictRepo;
import blueapps.lgs.repositories.SectorRepo;
import blueapps.lgs.repositories.UserRepository;
import blueapps.lgs.repositories.VillageRepo;
import blueapps.lgs.services.AuthService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AuthServiceImpl implements AuthService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    DistrictRepo districtRepository;
    @Autowired
    SectorRepo sectorRepository;
    @Autowired
    CellRepo cellRepository;
    @Autowired
    VillageRepo villageRepository;

    @Override
    public User getLoggedInUser() {
        log.info("{}");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        MyUserDetails userDetails = (MyUserDetails) authentication.getPrincipal();
        User user = userDetails.getUser();
        return user;
    }

    @Override
    public LocationResponse getLocation() {
        LocationResponse location = new LocationResponse();  

        User user = this.getLoggedInUser();
        Long id = (long) user.getLocationId();
        String locationType = user.getLocationType().toString();

        location.setLocationObject(this.getLocationObject());
        location.setLocationId(id);
        location.setLocationType(locationType);

        return location;
    }

    public Object getLocationObject() {
        User user = this.getLoggedInUser();
        Long id = (long) user.getLocationId();
        String locationType = user.getLocationType().toString();
        
        if (locationType.matches("DISTRICT")) {
            Optional<DistrictModel> district = districtRepository.findById(id);
            if (!district.isPresent()) {
                throw new ResourceNotFoundException("District  with id ::" + id + ":: doesn't exists");
            }
            return cast(district.get());
        } else if (locationType.matches("SECTOR")) {
            Optional<SectorModel> sector = sectorRepository.findById(id);
            if (!sector.isPresent()) {
                throw new ResourceNotFoundException("Sector  with id ::" + id + ":: doesn't exists");
            }
            return cast(sector.get());
        } else if (locationType.matches("CELL")) {
            Optional<CellModel> cell = cellRepository.findById(id);
            if (!cell.isPresent()) {
                throw new ResourceNotFoundException("Cell  with id ::" + id + ":: doesn't exists");
            }
            return cast(cell.get());
        } else if (locationType.matches("VILLAGE")) {
            Optional<VillageModel> village = villageRepository.findById(id);
            if (!village.isPresent()) {
                throw new ResourceNotFoundException("Village  with id ::" + id + ":: doesn't exists");
            }
            return cast(village.get());
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    private static <T extends Object> T cast(Object obj) {
        return (T) obj;
    }



}