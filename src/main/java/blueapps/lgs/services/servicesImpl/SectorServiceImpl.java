package blueapps.lgs.services.servicesImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import blueapps.lgs.exeptions.ResourceAlreadyExistsException;
import blueapps.lgs.exeptions.ResourceNotFoundException;
import blueapps.lgs.models.DistrictModel;
import blueapps.lgs.models.SectorModel;
import blueapps.lgs.models.dto.SectorDto;
import blueapps.lgs.repositories.DistrictRepo;
import blueapps.lgs.repositories.SectorRepo;
import blueapps.lgs.services.SectorService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SectorServiceImpl implements SectorService {
    @Autowired
    private SectorRepo sectorRepo;

    @Autowired
    DistrictRepo districtRepo;

    @Override
    public SectorDto createSector(SectorDto sectorDto, Long id) {
        DistrictModel district = districtRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("District not found"));
        SectorModel sector = sectorDto.mapToEntity();
        
        Optional<SectorModel> checkIfSectorExist = sectorRepo.findByNameContainingIgnoreCaseAndDistrict(sector.getName(), district);
        if(checkIfSectorExist.isPresent()){
            throw new ResourceAlreadyExistsException("Sector already recorded...");
        }
        
        sector.setDistrict(district);
        SectorModel createdSector = sectorRepo.save(sector);

        SectorDto returnValue = createdSector.mapToDto();

        return returnValue;
    }

    @Override
    public List<SectorDto> getSectors() {
        List<SectorDto> returnValue = new ArrayList<>();
        List<SectorModel> sectors = sectorRepo.findAll(PageRequest.of(0, Integer.MAX_VALUE)).getContent();

        for(SectorModel sector: sectors){
            SectorDto sectorDto =  sector.mapToDto();
            returnValue.add(sectorDto);
        }

        return returnValue;
    }

    @Override
    public SectorDto getSectorById(Long id) {
        
        SectorModel sector = sectorRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Sector not found"));
        SectorDto returnValue = sector.mapToDto();

        return returnValue;
    }

    @Override
    public List<SectorDto> getSectorsByDistrict(Long districtId) {
        List<SectorDto> returnValue = new ArrayList<>();
        DistrictModel district = districtRepo.findById(districtId).orElseThrow(() -> new ResourceNotFoundException("District not found"));
        List<SectorModel> sectorModels = sectorRepo.findByDistrict(district);

        for(SectorModel sectorModel: sectorModels){
            SectorDto sectorDto = sectorModel.mapToDto();
            returnValue.add(sectorDto);
        }
        return returnValue;
    }

    @Override
    public SectorDto updateSector(SectorDto sectorDto, Long id, Long districtId) {
        SectorModel sector = sectorRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Sector not found"));
        DistrictModel district = districtRepo.findById(districtId).orElseThrow(() -> new ResourceNotFoundException("District not found"));
        
        sector.setName(sectorDto.getName());
        sector.setSurface(sectorDto.getSurface());
        sector.setDistrict(district);
        
        SectorModel updatedSector = sectorRepo.save(sector);
        SectorDto returnValue = updatedSector.mapToDto();

        return returnValue;
    }
}
