package blueapps.lgs.services.servicesImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import blueapps.lgs.exeptions.ResourceAlreadyExistsException;
import blueapps.lgs.exeptions.ResourceNotFoundException;
import blueapps.lgs.models.Category;
import blueapps.lgs.models.CellModel;
import blueapps.lgs.models.DistrictModel;
import blueapps.lgs.models.Record;
import blueapps.lgs.models.SectorModel;
import blueapps.lgs.models.User;
import blueapps.lgs.models.VillageModel;
import blueapps.lgs.models.Response.RecordResponse;
import blueapps.lgs.models.dto.RecordDto;
import blueapps.lgs.repositories.CategoryRepository;
import blueapps.lgs.repositories.CellRepo;
import blueapps.lgs.repositories.DistrictRepo;
import blueapps.lgs.repositories.RecordRepository;
import blueapps.lgs.repositories.SectorRepo;
import blueapps.lgs.repositories.VillageRepo;
import blueapps.lgs.services.AuthService;
import blueapps.lgs.services.CategoryService;
import blueapps.lgs.services.CellService;
import blueapps.lgs.services.ClusterService;
import blueapps.lgs.services.HistoryService;
import blueapps.lgs.services.RecordService;
import blueapps.lgs.services.SectorService;
import blueapps.lgs.services.VillageService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RecordServiceImpl implements RecordService {

    @Autowired
    RecordRepository recordRepository;

    @Autowired
    CategoryService categoryService;

    @Autowired
    VillageService villageService;

    @Autowired
    CellService cellService;

    @Autowired
    SectorService sectorService;

    @Autowired
    DistrictRepo districtRepo;

    @Autowired
    AuthService authService;

    @Autowired
    HistoryService historyService;

    @Autowired
    SectorRepo sectorRepo;

    @Autowired
    CellRepo cellRepo;

    @Autowired
    VillageRepo villageRepo;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    ClusterService clusterService;

    @Override
    public RecordDto getRecordById(Long id) {
        Record record = recordRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Record not found"));
        // record.getCategory().getCluster().mapToDto().setRecords(clusterService.countRecordsByCluster(record.getCategory().getCluster()));
        return record.mapToDto();
    }

    @Override
    public List<RecordDto> getAllRecords(int page, int limit) {
        List<RecordDto> recordDtos = new ArrayList<>();
        if (page > 0)
            page = page - 1;
        List<Record> records = recordRepository.findAll(PageRequest.of(page, limit)).getContent();
        for (Record record : records) {
            recordDtos.add(record.mapToDto());
        }
        return recordDtos;
    }

    @Override
    public RecordDto createRecord(RecordDto recordDto, Long categoryId, Long villageId) {
        if (this.getRecordByNid(recordDto.getNid()).isPresent()) {
            throw new ResourceAlreadyExistsException("Record with NID ::" + recordDto.getNid() + ":: already exist");
        }
        Record record = recordDto.mapToRecord();
        Category Category = categoryRepository.findById(categoryId.intValue())
                .orElseThrow(() -> new ResourceNotFoundException("Category not found"));
        record.setCategory(Category);
        log.info("{}", "record");
        VillageModel village = villageRepo.findById(villageId)
                .orElseThrow(() -> new ResourceNotFoundException("Village not found"));
        record.setVillage(village);
        record.setStatus("active");
        Record recordToSave = recordRepository.save(record);
        if (recordToSave != null) {
            historyService.storeHistory(recordToSave.mapToDto(), "CREATE");
        }
        return recordToSave.mapToDto();
    }

    @Override
    public RecordDto updateRecord(RecordDto recordDto, Long recordId, Long categoryId, Long villageId) {
        Record record = recordRepository.findById(recordId)
                .orElseThrow(() -> new ResourceNotFoundException("Record not found"));
        record.setFirstName(recordDto.getFirstName());
        record.setLastName(recordDto.getLastName());
        record.setNid(recordDto.getNid());
        record.setContact(recordDto.getContact());
        record.setUbudehe(recordDto.getUbudehe());
        record.setDescription(recordDto.getDescription());
        log.info("{}", record);
        Category Category = categoryRepository.findById(categoryId.intValue())
                .orElseThrow(() -> new ResourceNotFoundException("Category not found"));
        record.setCategory(Category);
        VillageModel village = villageRepo.findById(villageId)
                .orElseThrow(() -> new ResourceNotFoundException("Village not found"));
        record.setVillage(village);
        Record recordToSave = recordRepository.save(record);
        if (recordToSave != null) {
            historyService.storeHistory(record.mapToDto(), "UPDATE");
        }
        return recordToSave.mapToDto();
    }

    @Override
    public Long getCounts(Integer id) throws ResourceNotFoundException {
        return null;
    }

    @Override
    public List<RecordDto> getRecordByLocation(String locationType, Integer locationId) {
        List<Record> records = new ArrayList<Record>();
        List<RecordDto> recordDtos = new ArrayList<>();
        if (locationType.toUpperCase().equals("VILLAGE")) {
            records = this.getRecordByVillage(locationId);
            for (Record record : records) {
                recordDtos.add(record.mapToDto());
            }
            return recordDtos;
        } else if (locationType.toUpperCase().equals("CELL")) {
            records = this.getRecordByCell(locationId);
            for (Record record : records) {
                recordDtos.add(record.mapToDto());
            }
            return recordDtos;
        } else if (locationType.toUpperCase().equals("SECTOR")) {
            records = this.getRecordBySector(locationId);
            for (Record record : records) {
                recordDtos.add(record.mapToDto());
            }
            return recordDtos;
        } else if (locationType.toUpperCase().equals("DISTRICT")) {
            records = this.getRecordByDistrict(locationId);
            for (Record record : records) {
                recordDtos.add(record.mapToDto());
            }
            return recordDtos;
        } else {
            return recordDtos;
        }
    }

    @Override
    public List<Record> getRecordByVillage(Integer locationId) {
        Long idLong = Long.parseLong(String.valueOf(locationId));
        VillageModel village = villageRepo.findById(idLong)
                .orElseThrow(() -> new ResourceNotFoundException("Village not found"));
        return recordRepository.findByVillage(village);
    }

    @Override
    public List<Record> getRecordByCell(Integer locationId) {
        List<Record> records = new ArrayList<Record>();
        Long idLong = Long.parseLong(String.valueOf(locationId));
        CellModel cell = cellRepo.findById(idLong)
                .orElseThrow(() -> new ResourceNotFoundException("Cell not found..."));
        List<VillageModel> villages = villageRepo.findByCell(cell);
        for (VillageModel village : villages) {
            int id = Integer.parseInt(String.valueOf(village.getId()));
            List<Record> recordByVillage = this.getRecordByVillage(id);
            records.addAll(recordByVillage);
        }
        return records;
    }

    @Override
    public List<Record> getRecordBySector(Integer locationId) {
        List<Record> records = new ArrayList<Record>();
        Long idLong = Long.parseLong(String.valueOf(locationId));
        SectorModel sector = sectorRepo.findById(idLong)
                .orElseThrow(() -> new ResourceNotFoundException("Sector not found"));

        List<CellModel> cells = cellRepo.findBySector(sector);
        for (CellModel cell : cells) {
            int id = Integer.parseInt(String.valueOf(cell.getId()));
            List<Record> result = this.getRecordByCell(id);
            records.addAll(result);
        }
        return records;
    }

    @Override
    public List<Record> getRecordByDistrict(Integer locationId) {
        List<Record> records = new ArrayList<Record>();
        Long idLong = Long.parseLong(String.valueOf(locationId));
        DistrictModel district = districtRepo.findById(idLong)
                .orElseThrow(() -> new ResourceNotFoundException("District not found"));
        List<SectorModel> sectors = sectorRepo.findByDistrict(district);
        log.info("{}", sectors);
        for (SectorModel sector : sectors) {
            int id = Integer.parseInt(String.valueOf(sector.getId()));
            List<Record> result = this.getRecordBySector(id);
            records.addAll(result);
        }
        return records;
    }

    public List<RecordDto> getRecordByAuthUser() {
        User authUser = authService.getLoggedInUser();
        String locationType = authUser.getLocationType().toString();
        int locationId = authUser.getLocationId();
        return this.getRecordByLocation(locationType, locationId);
    }

    @Override
    public List<RecordDto> recordsByCateg(Integer category) {
        List<RecordDto> recordDtos = new ArrayList<>();
        List<Record> records = recordRepository.recordsByCateg(category);
        log.info("{}", records);
        for (Record record : records) {
            recordDtos.add(record.mapToDto());
        }
        return recordDtos;
    }

    @Override
    public List<Record> getRecordByCategory(Category category) {
        return recordRepository.findByCategory(category);
    }

    @Override
    public List<Record> getRecordByCategoryAndVillage(Category category, Integer locationId) {
        Long idLong = Long.parseLong(String.valueOf(locationId));
        VillageModel village = villageRepo.findById(idLong)
                .orElseThrow(() -> new ResourceNotFoundException("Village not found"));
        return recordRepository.findByCategoryAndVillage(category, village);
    }

    @Override
    public Optional<Record> getRecordByNid(String nid) {
        return recordRepository.findByNid(nid);
    }

    @Override
    public Record deleteRecord(Long id) {
        return null;
    }

    @Override
    public RecordResponse mapDtoToResponse(RecordDto recordDto) {
        RecordResponse recordResponse = new RecordResponse();
        BeanUtils.copyProperties(recordDto, recordResponse);
        recordResponse.setVillage(recordDto.getVillage().mapToResponse());
        recordResponse.setCategory(categoryService.mapDtoToResponse(recordDto.getCategory()));
        return recordResponse;
    }

    // @Override
    // public Record deleteRecord(Long id) {
    // Record record = this.getRecordById(id);
    // if (record.getIsDeleted()) {
    // return record;
    // }
    // record.setIsDeleted(true);
    // Record recordToSave = recordRepository.save(record);
    // if (recordToSave != null) {
    // historyService.storeHistory(record, "DELETE");
    // }
    // return recordRepository.save(record);
    // }

}
