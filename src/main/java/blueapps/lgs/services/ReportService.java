package blueapps.lgs.services;

import java.util.List;

import blueapps.lgs.models.Response.ReportResponse;

public interface ReportService {
    List<ReportResponse> getReportByLocation(String locationType, Integer locationId);
    List<ReportResponse> getReports();
}
