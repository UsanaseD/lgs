
package blueapps.lgs.services;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import blueapps.lgs.models.ClusterModel;
import blueapps.lgs.models.Response.ClusterResponse;
import blueapps.lgs.models.dto.ClusterDto;

public interface ClusterService {
    
    public ClusterDto createCluster(ClusterDto clusterDto);

    public ResponseEntity<?> getErrors(BindingResult bindingResult);

    public Optional<ClusterModel> emailExst(String email);

    public ClusterDto getClusterById(Integer id);
    
    public List<ClusterDto> getAllClusters(int page, int limit);

    public ClusterDto updateCluster(ClusterDto clusterDto, Integer id);

    public int countRecordsByCluster(ClusterModel cluster);

    public ClusterResponse mapDtoToResponse(ClusterDto clusterDto);
    }
