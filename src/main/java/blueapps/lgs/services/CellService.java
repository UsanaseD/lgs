package blueapps.lgs.services;

import java.util.List;

import blueapps.lgs.models.dto.CellDto;

public interface CellService {
    CellDto createCell(CellDto cellDto, Long sectorId);
    CellDto updateCell(CellDto cellDto, Long id, Long sectorId);
    CellDto getCellById(Long id);
    List<CellDto> getCells();
    List<CellDto> getCellsBySector(Long sectorId);
}
