package blueapps.lgs.services;

import java.util.List;

import blueapps.lgs.models.dto.DistrictDto;

public interface DistrictService {
    public DistrictDto createDistrict(DistrictDto districtDto);
    public List<DistrictDto> getDistricts();
    public DistrictDto getDistrict(Long id);
    public List<DistrictDto> getDistrictsByProvince(String province);
    public DistrictDto updateDistrict(DistrictDto districtDto, Long id);
}