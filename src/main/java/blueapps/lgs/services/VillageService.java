package blueapps.lgs.services;

import java.util.List;

import blueapps.lgs.models.dto.VillageDto;

public interface VillageService {
    VillageDto createVillage(VillageDto villageDto, Long cellId);
    VillageDto getVillageById(Long villageId);
    VillageDto updateVillage(VillageDto villageDto, Long villageId, Long cellId);
    List<VillageDto> getVillages();
    List<VillageDto> getVillagesByCell(Long cellId);
}
