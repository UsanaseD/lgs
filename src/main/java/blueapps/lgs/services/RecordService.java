package blueapps.lgs.services;

import java.util.List;
import java.util.Optional;

import blueapps.lgs.exeptions.ResourceNotFoundException;
import blueapps.lgs.models.Category;
import blueapps.lgs.models.Record;
import blueapps.lgs.models.Response.RecordResponse;
import blueapps.lgs.models.dto.RecordDto;

public interface RecordService {
    RecordDto createRecord(RecordDto recordDto, Long categoryId, Long villageId);

    RecordDto updateRecord(RecordDto recordDto, Long recordId, Long categoryId, Long villageId);

    Record deleteRecord(Long id);

    RecordDto getRecordById(Long id);

    Optional<Record> getRecordByNid(String nid);

    List<RecordDto> getRecordByLocation(String locationType, Integer locationId);

    List<Record> getRecordByVillage(Integer locationId);

    List<Record> getRecordByCell(Integer locationId);

    List<Record> getRecordBySector(Integer locationId);

    List<Record> getRecordByDistrict(Integer locationId);

    List<RecordDto> getRecordByAuthUser();

    List<RecordDto> recordsByCateg(Integer category);

    List<Record> getRecordByCategory(Category category);

    List<Record> getRecordByCategoryAndVillage(Category category, Integer locationId);

    List<RecordDto> getAllRecords(int page, int limit);

    Long getCounts(Integer id);

    RecordResponse mapDtoToResponse(RecordDto recordDto);
}
