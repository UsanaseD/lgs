package blueapps.lgs.services;

import java.util.List;

import blueapps.lgs.models.Response.CategoryResponse;
import blueapps.lgs.models.dto.CategoryDto;

public interface CategoryService {
    CategoryDto createCategory(CategoryDto categoryDto, Integer clusterId);
    CategoryDto updateCategory(CategoryDto categoryDto, Integer clusterId, Integer id);
    CategoryDto getCategoryById(Integer id);
    CategoryResponse mapDtoToResponse(CategoryDto categoryDto);
    List<CategoryDto> getAllCategories(int page, int limit);
    List<CategoryDto> getCategoriesParOwner(Integer id);
}
