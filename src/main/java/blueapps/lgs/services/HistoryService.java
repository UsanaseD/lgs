package blueapps.lgs.services;

import java.util.List;

import blueapps.lgs.models.History;
import blueapps.lgs.models.dto.RecordDto;

public interface HistoryService {
    History storeHistory(RecordDto record, String action);
    List<History> getAllHistory(int page, int limit);
    List<History> getHistoryByCreatorId(Long creatorId);
}
