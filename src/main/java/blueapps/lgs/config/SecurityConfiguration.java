package blueapps.lgs.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import blueapps.lgs.filters.CORSFilter;
import blueapps.lgs.filters.JWTRequestFilter;
import blueapps.lgs.services.MyUserDetailsService;
import blueapps.lgs.util.CustomLogoutHandler;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    MyUserDetailsService userDetailsService;

    @Autowired
    JWTRequestFilter jwtFilter;

    @Autowired
    CORSFilter corsFilter;

    @Autowired
    CustomLogoutHandler customLogoutHandler;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(userDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                .antMatchers("/", "/api/v1/auth/authenticate", "/swagger-ui/**", "/swagger-resources/**",
                        "/v2/api-docs")
                .permitAll()
                .antMatchers(HttpMethod.POST, "/api/v1/district", "/api/v1/cell", "/api/v1/village", "/api/v1/sector",
                        "/api/v1/users", "/api/v1/categories", "/api/v1/cluster")
                .hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/api/v1/records")
                .hasRole("SOCIAL_AFFAIRS")
                .antMatchers(HttpMethod.PUT, "/api/v1/village/{villageId}", "/api/v1/cell/{id}",
                        "/api/v1/sector/{id}", "/api/v1/records/{id}",
                        "/api/v1/categories/{id}", "/api/v1/district/{id}", "/api/v1/users/{id}")
                .permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/village", "/api/v1/district/prov/", "/api/v1/district",
                        "/api/v1/sector/dist/{distId}", "/api/v1/sector", "/api/v1/cell/sector/{sectId}",
                        "/api/v1/village/cell/{cellId}",
                        "/api/v1/histories", "/api/v1/histories/creator/{id}", "/api/v1/cluster/creator/{id}",
                        "/api/v1/cluster/counts/{id}", "/api/v1/categories/counts/{id}", "/api/v1/records/counts/{id}",
                        "/api/v1/categories/user/{id}", "/api/v1/categories", "/api/v1/categories/{id}",
                        "/api/v1/categories/currentMonth/{id}", "/api/v1/records/{id}","/api/v1/records",
                        "/api/v1/categories/user/location/{id}",
                        "/api/v1/reports/{locationType}/{locationId}","/api/v1/reports",
                        "/api/v1/village/cell/{cellId}", "/api/v1/village/cell/sector/{sectorId}",
                        "/api/v1/records/categories/{id}", "/api/v1/records/{locationType}/{locationId}",
                        "/api/v1/cell","api/v1/users","api/v1/categories",
                        "/api/v1/cluster")
                .permitAll()
                .anyRequest().authenticated()
                .and().sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
        http.addFilterBefore(corsFilter, ChannelProcessingFilter.class);
        http.csrf().disable();

        http.headers().frameOptions().disable();

        http.logout().logoutSuccessHandler(customLogoutHandler);

        http.cors();
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}