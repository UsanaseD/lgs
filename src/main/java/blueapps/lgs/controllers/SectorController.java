package blueapps.lgs.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import blueapps.lgs.models.Request.SectorRequest;
import blueapps.lgs.models.Response.SectorResponse;
import blueapps.lgs.models.dto.SectorDto;
import blueapps.lgs.services.DistrictService;
import blueapps.lgs.services.SectorService;
import blueapps.lgs.util.ApiResponse;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("api/v1/sector")
@Log4j2
public class SectorController {
    @Autowired
    private SectorService sectorService;

    @Autowired
    DistrictService districtService;

    @PostMapping
    public ResponseEntity<ApiResponse<SectorResponse>> store(@Valid @RequestBody SectorRequest sectorRequest,
            BindingResult bindingResult) {
                // Check validation errors
        if (bindingResult.hasErrors()) {
            String errors = bindingResult.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining("::"));
            throw new ValidationException(errors);
        }
        
        SectorDto sectorDto = sectorRequest.mapToDto();

        SectorDto sector = sectorService.createSector(sectorDto, sectorRequest.getDistrictId());
        log.info("{}", sector);

        SectorResponse response = sector.mapToResponse();

        final String message = "Sector created successfully...";
        ApiResponse<SectorResponse> body = new ApiResponse<>(HttpStatus.CREATED, message, response);
        return ResponseEntity.status(HttpStatus.CREATED).body(body);
    }

    @GetMapping
    public ResponseEntity<ApiResponse<List<SectorResponse>>> index(){
        List<SectorResponse> sectorResponses = new ArrayList<>();

        List<SectorDto> sectorDtos = sectorService.getSectors();
        log.info("{}", sectorDtos);

        for(SectorDto sector: sectorDtos){
            SectorResponse sectorResponse = sector.mapToResponse();
            sectorResponses.add(sectorResponse);
        }

        final String message = "Sectors retrieved successfully...";
        ApiResponse<List<SectorResponse>> body = new ApiResponse<>(HttpStatus.CREATED, message, sectorResponses);
        return ResponseEntity.ok(body);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse<SectorResponse>> show(@PathVariable("id") Long id){
        
        SectorDto sectorDto = sectorService.getSectorById(id);

        SectorResponse sectorResponse = sectorDto.mapToResponse();

        final String message = "Sector retrieved successfully...";
        ApiResponse<SectorResponse> body = new ApiResponse<>(HttpStatus.CREATED, message, sectorResponse);
        return ResponseEntity.ok(body);
    }

    @GetMapping("district/{districtId}")
    public ResponseEntity<ApiResponse<List<SectorResponse>>> sectorsByDist(@PathVariable long districtId) {
        List<SectorResponse> sectorResponses = new ArrayList<>();
        List<SectorDto> sectors = sectorService.getSectorsByDistrict(districtId);
        
        for(SectorDto sectorDto: sectors){
            SectorResponse sectorResponse = sectorDto.mapToResponse();
            sectorResponses.add(sectorResponse);
        }

        final String message = "sectors retrieved successfully...";
        ApiResponse<List<SectorResponse>> body = new ApiResponse<>(HttpStatus.OK, message, sectorResponses);
        return ResponseEntity.ok(body);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ApiResponse<SectorResponse>> update(@PathVariable Long id, @Valid @RequestBody SectorRequest sectorRequest,
    BindingResult bindingResult) {
        // Check validation errors
        if (bindingResult.hasErrors()) {
            String errors = bindingResult.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining("::"));
            throw new ValidationException(errors);
        }
        
        
        SectorDto sectorDto = sectorRequest.mapToDto();
        
        SectorDto updatSectorDto = sectorService.updateSector(sectorDto, id, sectorRequest.getDistrictId());
        
        SectorResponse sectorResponse = updatSectorDto.mapToResponse();

        final String message = "Sector updated successfully...";
        ApiResponse<SectorResponse> body = new ApiResponse<>(HttpStatus.CREATED, message, sectorResponse);
        return ResponseEntity.status(HttpStatus.CREATED).body(body);
    }
}
