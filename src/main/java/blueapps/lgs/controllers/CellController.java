package blueapps.lgs.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import blueapps.lgs.models.Request.CellRequest;
import blueapps.lgs.models.Response.CellResponse;
import blueapps.lgs.models.dto.CellDto;
import blueapps.lgs.services.CellService;
import blueapps.lgs.util.ApiResponse;
import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
@RequestMapping("api/v1/cell")
public class CellController {
    @Autowired
    private CellService cellService;

    @PostMapping
    public ResponseEntity<ApiResponse<CellResponse>> store(@Valid @RequestBody CellRequest cellRequest,
            BindingResult bindingResult) {
        // Check validation errors
        if (bindingResult.hasErrors()) {
            String errors = bindingResult.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining("::"));
            throw new ValidationException(errors);
        }
        
        
        CellDto cellDto = cellRequest.mapToDto();
        log.info("{}", cellDto);
        CellDto cellToCreate = cellService.createCell(cellDto, cellRequest.getSectorId());
        final String message = "Cell created successfully...";
        ApiResponse<CellResponse> body = new ApiResponse<>(HttpStatus.OK, message, cellToCreate.mapToResponse());
        return ResponseEntity.status(HttpStatus.CREATED).body(body);
    }

    @GetMapping
    public ResponseEntity<ApiResponse<List<CellResponse>>> index() {
        List<CellResponse> cellResponses = new ArrayList<>();

        List<CellDto> cellDtos = cellService.getCells();

        for (CellDto cellDto : cellDtos) {
            CellResponse cellResponse = cellDto.mapToResponse();
            cellResponses.add(cellResponse);
        }

        final String message = "Cells retrieved successfully...";
        ApiResponse<List<CellResponse>> body = new ApiResponse<>(HttpStatus.OK, message, cellResponses);
        return ResponseEntity.ok(body);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse<CellResponse>> show(@PathVariable("id") Long id) {
        CellDto cellDto = cellService.getCellById(id);
        CellResponse cellResponse = cellDto.mapToResponse();
        final String message = "Cell retrieved successfully...";
        ApiResponse<CellResponse> body = new ApiResponse<>(HttpStatus.OK, message, cellResponse);
        return ResponseEntity.ok(body);
    }

    @GetMapping("/sector/{sectId}")
    public ResponseEntity<ApiResponse<List<CellResponse>>> cellsBySect(@PathVariable Long sectId) {
        List<CellResponse> cellResponses = new ArrayList<>();
        List<CellDto> cellDtos = cellService.getCellsBySector(sectId);

        for (CellDto cellDto : cellDtos) {
            CellResponse cellResponse = cellDto.mapToResponse();
            cellResponses.add(cellResponse);
        }

        final String message = "Cells retrieved successfully...";
        ApiResponse<List<CellResponse>> body = new ApiResponse<>(HttpStatus.OK, message, cellResponses);
        return ResponseEntity.ok(body);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ApiResponse<CellResponse>> update(@Valid @RequestBody CellRequest cellRequest, @PathVariable("id") Long id,
            BindingResult bindingResult) {
        // Check validation errors
        if (bindingResult.hasErrors()) {
            String errors = bindingResult.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining("::"));
            throw new ValidationException(errors);
        }
        
        CellDto cellDto = cellRequest.mapToDto();
        CellDto cellToUpdate = cellService.updateCell(cellDto, id, cellRequest.getSectorId() );

        CellResponse cellResponse = cellToUpdate.mapToResponse();

        final String message = "Cells retrieved successfully...";
        ApiResponse<CellResponse> body = new ApiResponse<>(HttpStatus.OK, message, cellResponse);
        return ResponseEntity.status(HttpStatus.CREATED).body(body);

    }
}
