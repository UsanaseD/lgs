package blueapps.lgs.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import blueapps.lgs.models.Request.VillageRequest;
import blueapps.lgs.models.Response.VillageResponse;
import blueapps.lgs.models.dto.VillageDto;
import blueapps.lgs.services.VillageService;
import blueapps.lgs.util.ApiResponse;
import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
@RequestMapping("api/v1/village")
public class VillageController {
    @Autowired
    private VillageService villageService;

    @PostMapping
    public ResponseEntity<ApiResponse<VillageResponse>> create(@Valid @RequestBody VillageRequest villageRequest, BindingResult bindingResult) {
        // Check validation errors
        if (bindingResult.hasErrors()) {
            String errors = bindingResult.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining("::"));
            throw new ValidationException(errors);
        }
        
        VillageDto villageDto = villageService.createVillage(villageRequest.mapToDto(), villageRequest.getCellId());
        VillageResponse villageResponse = villageDto.mapToResponse();

        final String message = "Village created successfully...";
        ApiResponse<VillageResponse> body = new ApiResponse<>(HttpStatus.OK, message,
        villageResponse);
        return ResponseEntity.status(HttpStatus.CREATED).body(body);
    }

    @GetMapping()
    public ResponseEntity<ApiResponse<List<VillageResponse>>> index(){
        List<VillageResponse> villageResponses = new ArrayList<>();
        List<VillageDto> villageDtos = villageService.getVillages();

        for (VillageDto villageDto : villageDtos) {
            villageResponses.add(villageDto.mapToResponse());
        }

        final String message = "Villages retrieved successfully...";
        ApiResponse<List<VillageResponse>> body = new ApiResponse<>(HttpStatus.OK, message,
        villageResponses);
        return ResponseEntity.ok(body);
    }

    @GetMapping("/{villageId}")
    public ResponseEntity<ApiResponse<VillageResponse>> show(@PathVariable Long villageId){
        VillageDto villageDto = villageService.getVillageById(villageId);
        VillageResponse villageResponse = villageDto.mapToResponse();

        final String message = "Village retrieved successfully...";
        ApiResponse<VillageResponse> body = new ApiResponse<>(HttpStatus.OK, message,
        villageResponse);
        return ResponseEntity.ok(body);
    }

    @PutMapping("/{villageId}")
    public ResponseEntity<ApiResponse<VillageResponse>> update(@Valid @RequestBody VillageRequest villageRequest, BindingResult bindingResult, @PathVariable Long villageId){
        // Check validation errors
        if (bindingResult.hasErrors()) {
            String errors = bindingResult.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining("::"));
            throw new ValidationException(errors);
        }
        
        VillageDto updatedVillageDto = villageService.updateVillage(villageRequest.mapToDto(), villageId, villageRequest.getCellId());
        VillageResponse villageResponse = updatedVillageDto.mapToResponse();

        final String message = "Village retrieved successfully...";
        ApiResponse<VillageResponse> body = new ApiResponse<>(HttpStatus.OK, message,
        villageResponse);
        return ResponseEntity.status(HttpStatus.CREATED).body(body);
    }

    @GetMapping("/cell/{cellId}")
    public ResponseEntity<ApiResponse<List<VillageResponse>>> villageByCell(@PathVariable("cellId") Long cellId){
        List<VillageDto> villageDtos = villageService.getVillagesByCell(cellId);
        List<VillageResponse> villageResponses = new ArrayList<>();
        for (VillageDto villageDto : villageDtos) {
            villageResponses.add(villageDto.mapToResponse());
        }
        final String message = "Villages retrieved successfully...";
        ApiResponse<List<VillageResponse>> body = new ApiResponse<>(HttpStatus.OK, message,
        villageResponses);
        return ResponseEntity.ok(body);
    }
}
