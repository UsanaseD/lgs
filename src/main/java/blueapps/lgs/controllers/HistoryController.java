package blueapps.lgs.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import blueapps.lgs.exeptions.ResourceNotFoundException;
import blueapps.lgs.models.History;
import blueapps.lgs.models.Response.HistoryResponse;
import blueapps.lgs.models.dto.HistoryDto;
import blueapps.lgs.services.HistoryService;
import blueapps.lgs.util.ApiResponse;
import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
@RequestMapping("api/v1/histories")
public class HistoryController {

    @Autowired
    HistoryService historyService;
    
    @GetMapping(" ")
    public ResponseEntity<ApiResponse<List<History>>> index(@RequestParam(value="page", defaultValue="1") int page,@RequestParam(value="limit", defaultValue="25") int limit){
        List<History> histories =  historyService.getAllHistory(page, limit);
        // for (HistoryDto historyDto : historyDtos) {
        //     histories.add(historyDto.mapToResponse());
        // }
        final String message = "History retrieved successfully...";
        ApiResponse<List<History>> body = new ApiResponse<>(HttpStatus.OK, message, histories);
        return ResponseEntity.ok(body);
    }


    @GetMapping("/creator/{creatorId}")
    public ResponseEntity<ApiResponse<List<History>>> historyByCreator(@PathVariable("creatorId") Long creatorId){
        List<History> histories =  historyService.getHistoryByCreatorId(creatorId);
        final String message = "History retrieved successfully...";
        ApiResponse<List<History>> body = new ApiResponse<>(HttpStatus.OK, message, histories);
        return ResponseEntity.ok(body);
    }
}
