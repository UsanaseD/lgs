package blueapps.lgs.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import blueapps.lgs.models.Response.ReportResponse;
import blueapps.lgs.services.CategoryService;
import blueapps.lgs.services.RecordService;
import blueapps.lgs.services.ReportService;
import blueapps.lgs.util.ApiResponse;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/api/v1/reports")
public class ReportController {

    @Autowired
    CategoryService categoryService;

    @Autowired
    RecordService recordService;

    @Autowired
    ReportService reportService;

    @GetMapping("/{locationType}/{locationId}")
    public ResponseEntity<ApiResponse<List<ReportResponse>>> reportByLocation(@PathVariable("locationType") String locationType,@PathVariable("locationId") Integer locationId) {
        final String message = "Reports retrieved successfully...";
        List<ReportResponse> report = reportService.getReportByLocation(locationType, locationId);
        ApiResponse<List<ReportResponse>> body = new ApiResponse<>(HttpStatus.OK, message, report );
        return ResponseEntity.ok(body);
    }

    @GetMapping("")
    public ResponseEntity<ApiResponse<List<ReportResponse>>> index() {
        log.info("{}", reportService.getReports());
        final String message = "Reports retrieved successfully...";
        List<ReportResponse> report = reportService.getReports();
        ApiResponse<List<ReportResponse>> body = new ApiResponse<>(HttpStatus.OK, message, report );
        return ResponseEntity.ok(body);
    }
}
