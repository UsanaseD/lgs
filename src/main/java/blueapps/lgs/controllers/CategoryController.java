package blueapps.lgs.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import blueapps.lgs.models.Request.CategoryRequest;
import blueapps.lgs.models.Response.CategoryResponse;
import blueapps.lgs.models.dto.CategoryDto;
import blueapps.lgs.services.CategoryService;
import blueapps.lgs.util.ApiResponse;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("api/v1/categories")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @GetMapping("")
    public ResponseEntity<ApiResponse<List<CategoryResponse>>> index(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "limit", defaultValue = "25") int limit) {
        List<CategoryResponse> categories = new ArrayList<>();
        List<CategoryDto> categoriesDtos = categoryService.getAllCategories(page, limit);

        for (CategoryDto categoryDto : categoriesDtos) {
            categories.add(categoryService.mapDtoToResponse(categoryDto));
        }

        final String message = "The categories retrieved successfully ...";
        ApiResponse<List<CategoryResponse>> body = new ApiResponse<>(HttpStatus.OK, message, categories);

        return ResponseEntity.ok(body);
    }

    // @GetMapping("/api/v1/categories/currentMonth/{id}")
    // public ResponseEntity<ApiResponse<List<CategoryRequest>>> index(
    
    // )

    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse<CategoryResponse>> show(@PathVariable("id") int id) {

        CategoryDto categoryDto = categoryService.getCategoryById(id);
        CategoryResponse categoryResponse = categoryDto.mapToResponse();

        final String message = "The category retrieved successfully ..";
        ApiResponse<CategoryResponse> body = new ApiResponse<>(HttpStatus.OK, message, categoryResponse);

        return ResponseEntity.ok(body);
    }

    @PostMapping("")
    public ResponseEntity<ApiResponse<CategoryResponse>> store(
     @Valid @RequestBody CategoryRequest category, BindingResult result) {

        // Check validation errors
        if (result.hasErrors()) {
            String errors = result.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining("::"));
            log.info("{}", errors);        
            throw new ValidationException(errors); 
        }

        CategoryDto categoryDto = categoryService.createCategory(category.mapToDto(), category.getClusterId());
        CategoryResponse createdCategory = categoryDto.mapToResponse();

        final String message = "Category created successfully..";
        ApiResponse<CategoryResponse> body = new ApiResponse<>(HttpStatus.CREATED, message, createdCategory);

        return ResponseEntity.status(HttpStatus.CREATED).body(body);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ApiResponse<CategoryResponse>> update(@RequestBody CategoryRequest categoryRequest, BindingResult result,
            @PathVariable("id") Integer id) {
        if (result.hasErrors()) {
            String errors = result.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining("::"));
            throw new ValidationException(errors);
        }

        CategoryDto categoryDto = categoryService.updateCategory(categoryRequest.mapToDto(), categoryRequest.getClusterId(), id);
        CategoryResponse categoryResponse = categoryDto.mapToResponse();

        final String message = "Category updated successfully..";
        ApiResponse<CategoryResponse> body = new ApiResponse<>(HttpStatus.OK, message, categoryResponse);

        return ResponseEntity.status(HttpStatus.CREATED).body(body);
    }

}
