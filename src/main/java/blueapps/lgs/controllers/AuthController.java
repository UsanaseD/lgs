package blueapps.lgs.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import blueapps.lgs.config.JWTConfig;
import blueapps.lgs.models.AuthenticationRequest;
import blueapps.lgs.models.Response.LocationResponse;
import blueapps.lgs.services.AuthService;
import blueapps.lgs.services.MyUserDetailsService;
import blueapps.lgs.util.ApiResponse;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("api/v1/auth")
public class AuthController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JWTConfig jwtTokenUtil;

	@Autowired
	private MyUserDetailsService userDetailsService;

	@Autowired
	AuthService authService;

	@GetMapping("/location")
	public ResponseEntity<ApiResponse<LocationResponse>> getAuthLocation() {
		LocationResponse location = authService.getLocation();
		log.info("{}");

		final String message = "User location retrieved...";

		ApiResponse<LocationResponse> body = new ApiResponse<>(HttpStatus.OK, message, location);

		return ResponseEntity.ok(body);
	}

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<ApiResponse<String>> createAuthenticationToken(
			@RequestBody AuthenticationRequest authenticationRequest, Authentication authentication,
			HttpSession session) throws Exception {

		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getEmail(),
					authenticationRequest.getPassword()));

		} catch (BadCredentialsException e) {
			final String message = "Incorrect username or password...";

			ApiResponse<String> body = new ApiResponse<>(HttpStatus.UNAUTHORIZED, message, e.getMessage());

			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(body);
		}

		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getEmail());
		final String jwt = jwtTokenUtil.generateToken(userDetails);
		final String message = "Users logged in successfully...";

		ApiResponse<String> body = new ApiResponse<>(HttpStatus.OK, message, jwt);

		return ResponseEntity.ok(body);
	}
}
