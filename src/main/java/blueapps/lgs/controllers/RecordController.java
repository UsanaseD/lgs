package blueapps.lgs.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import blueapps.lgs.exeptions.ResourceNotFoundException;
import blueapps.lgs.models.Record;
import blueapps.lgs.models.Request.RecordRequest;
import blueapps.lgs.models.Response.RecordResponse;
import blueapps.lgs.models.dto.RecordDto;
import blueapps.lgs.services.RecordService;
import blueapps.lgs.util.ApiResponse;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("/api/v1/records")
public class RecordController {

    @Autowired
    RecordService recordService;

    @GetMapping("")
    public ResponseEntity<ApiResponse<List<RecordResponse>>> index(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "limit", defaultValue = "25") int limit) {
        List<RecordResponse> recordResponses = new ArrayList<>();
        List<RecordDto> records = recordService.getAllRecords(page, limit);
        for (RecordDto recordDto : records) {
            recordResponses.add(recordService.mapDtoToResponse(recordDto));
        }

        final String message = "The records retrieved successfully ...";
        ApiResponse<List<RecordResponse>> body = new ApiResponse<>(HttpStatus.OK, message, recordResponses);

        return ResponseEntity.ok(body);
    }

    @GetMapping("/counts/{id}")
    public ResponseEntity<ApiResponse<Long>> counts(@PathVariable("id") Integer id) throws ResourceNotFoundException {
        Long counts = recordService.getCounts(id);
        final String message = " The total Records are......";
        ApiResponse<Long> body = new ApiResponse<>(HttpStatus.OK, message, counts);
        return ResponseEntity.ok(body);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse<RecordResponse>> show(@PathVariable("id") Long id) {

        RecordDto recordDto = recordService.getRecordById(id);
        RecordResponse recordResponse = recordService.mapDtoToResponse(recordDto);

        final String message = "The Record retrieved successfully ..";
        ApiResponse<RecordResponse> body = new ApiResponse<>(HttpStatus.OK, message, recordResponse);

        return ResponseEntity.ok(body);
    }

    @GetMapping("/categories/{id}")
    public ResponseEntity<ApiResponse<List<RecordResponse>>> recordsParCategory(@PathVariable("id") Integer category) {
        log.info("{}", category);
        List<RecordResponse> recordResponses = new ArrayList<>();
        List<RecordDto> records = recordService.recordsByCateg(category);
        for (RecordDto recordDto : records) {
            recordResponses.add(recordService.mapDtoToResponse(recordDto));
        }
        final String message = "The Record retrieved successfully ..";
        ApiResponse<List<RecordResponse>> body = new ApiResponse<>(HttpStatus.OK, message, recordResponses);
        return ResponseEntity.ok(body);
    }

    @PostMapping("")
    public ResponseEntity<ApiResponse<RecordResponse>> store(@Valid @RequestBody RecordRequest recordRequest,
            Errors result) {
        if (result.hasErrors()) {
            String errors = result.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining("::"));
            throw new ValidationException(errors);
        }

        RecordDto recordDto = recordService.createRecord(recordRequest.mapToDto(), recordRequest.getCategoryId(),
                recordRequest.getVillageId());

        String message = "Record created successfully..";
        ApiResponse<RecordResponse> body = new ApiResponse<>(HttpStatus.CREATED, message,
                recordService.mapDtoToResponse(recordDto));

        return ResponseEntity.status(HttpStatus.CREATED).body(body);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ApiResponse<RecordResponse>> update(@RequestBody RecordRequest recordRequest, Errors result,
            @PathVariable("id") Long id) {
        if (result.hasErrors()) {
            String errors = result.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining("::"));
            throw new ValidationException(errors);
        }

        RecordDto recordDto = recordService.updateRecord(recordRequest.mapToDto(), id, recordRequest.getCategoryId(),
                recordRequest.getVillageId());

        final String message = "Record updated successfully..";
        ApiResponse<RecordResponse> body = new ApiResponse<>(HttpStatus.OK, message,
                recordService.mapDtoToResponse(recordDto));

        return ResponseEntity.status(HttpStatus.OK).body(body);
    }

    // @DeleteMapping("/{id}")
    // public ResponseEntity<ApiResponse<Record>> delete (@PathVariable("id") int
    // id) {
    // final Record RecordToUpdate = recordService.deleteRecord(id);

    // final String message = "Record delete successfully..";
    // ApiResponse<Record> body = new ApiResponse<>(HttpStatus.OK, message,
    // RecordToUpdate);

    // return ResponseEntity.status(HttpStatus.OK).body(body);
    // }

    @GetMapping("/{locationType}/{locationId}")
    public ResponseEntity<ApiResponse<List<RecordResponse>>> getRecordByLocation(
            @PathVariable("locationType") String locationType, @PathVariable("locationId") int locationId) {
        List<RecordResponse> recordResponses = new ArrayList<>();
        List<RecordDto> records = recordService.getRecordByLocation(locationType, locationId);
        for (RecordDto recordDto : records) {
            recordResponses.add(recordService.mapDtoToResponse(recordDto));
        }

        final String message = "The records retrieved successfully ...";
        ApiResponse<List<RecordResponse>> body = new ApiResponse<>(HttpStatus.OK, message, recordResponses);
        log.info("{}", body);

        return ResponseEntity.ok(body);
    }

    @GetMapping("/auth")
    public ResponseEntity<ApiResponse<List<RecordResponse>>> getRecordByAuthUser() {
        List<RecordDto> records = recordService.getRecordByAuthUser();

        List<RecordResponse> recordResponses = new ArrayList<>();
        for (RecordDto recordDto : records) {
            recordResponses.add(recordService.mapDtoToResponse(recordDto));
        }

        final String message = "The records retrieved successfully ...";
        ApiResponse<List<RecordResponse>> body = new ApiResponse<>(HttpStatus.OK, message, recordResponses);
        log.info("{}", body);

        return ResponseEntity.ok(body);
    }
}
