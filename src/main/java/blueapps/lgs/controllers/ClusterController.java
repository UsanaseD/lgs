
package blueapps.lgs.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import blueapps.lgs.models.Request.ClusterRequest;
import blueapps.lgs.models.Response.ClusterResponse;
import blueapps.lgs.models.dto.ClusterDto;
import blueapps.lgs.services.ClusterService;
import blueapps.lgs.util.ApiResponse;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("api/v1/cluster")
public class ClusterController {
    @Autowired
    ClusterService clusterService;
    
    @PostMapping("")
    public ResponseEntity<ApiResponse<ClusterResponse>> saveCluster(@Valid @RequestBody ClusterRequest clusterRequest, BindingResult result) {
        if (result.hasErrors()) {
            String errors = result.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining("::"));
            throw new ValidationException(errors);
        }

        ClusterDto clusterDto = clusterService.createCluster(clusterRequest.mapToDto());
        ClusterResponse cluster = clusterService.mapDtoToResponse(clusterDto);
        final String message = "Cluster created Successfully.....";
        ApiResponse<ClusterResponse> body = new ApiResponse<>(HttpStatus.CREATED, message, cluster);
        return ResponseEntity.status(HttpStatus.CREATED).body(body);
    }

    @GetMapping("")
    public ResponseEntity<ApiResponse<List<ClusterResponse>>> index(@RequestParam(value="page", defaultValue="1") int page,@RequestParam(value="limit", defaultValue="25") int limit) {
        List<ClusterResponse> clusterResponses = new ArrayList<>();
        List<ClusterDto> clusters = clusterService.getAllClusters(page, limit);

        for (ClusterDto clusterDto : clusters) {
            clusterResponses.add(clusterService.mapDtoToResponse(clusterDto));
        }

        final String message = "The clusters retrieved successfully ...";
        ApiResponse<List<ClusterResponse>> body = new ApiResponse<>(HttpStatus.OK, message, clusterResponses);

        return ResponseEntity.ok(body);
    }


    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse<ClusterResponse>> show(@PathVariable("id") Integer id) {

        ClusterDto clusterDto = clusterService.getClusterById(id);
        ClusterResponse cluster = clusterService.mapDtoToResponse(clusterDto);

        final String message = "The cluster retrieved successfully ..";
        ApiResponse<ClusterResponse> body = new ApiResponse<>(HttpStatus.OK, message, cluster);

        return ResponseEntity.ok(body);
    }
    
    @PutMapping("/{id}")
    public ResponseEntity<ApiResponse<ClusterResponse>> update(@Valid @RequestBody ClusterRequest clusterRequest, BindingResult result, @PathVariable("id") Integer id) {
        if (result.hasErrors()) {
            String errors = result.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining("::"));
            throw new ValidationException(errors);
        }
        ClusterDto clusterDto = clusterService.updateCluster(clusterRequest.mapToDto(), id);
        ClusterResponse clusterResponse = clusterService.mapDtoToResponse(clusterDto);

        final String message = "Cluster updated successfully..";
        ApiResponse<ClusterResponse> body = new ApiResponse<>(HttpStatus.OK, message, clusterResponse);

        return ResponseEntity.status(HttpStatus.CREATED).body(body);
    }
}
