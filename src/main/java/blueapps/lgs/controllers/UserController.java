package blueapps.lgs.controllers;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import blueapps.lgs.exeptions.ResourceNotFoundException;
import blueapps.lgs.models.User;
import blueapps.lgs.models.dto.UserDto;
import blueapps.lgs.services.UserService;
import blueapps.lgs.util.ApiResponse;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequestMapping("api/v1/users")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("")
    public ResponseEntity<ApiResponse<List<User>>> index(@RequestParam(value="page", defaultValue="1") int page,@RequestParam(value="limit", defaultValue="25") int limit) {

        final List<User> users = userService.getAllUsers(page, limit);

        final String message = "Users retrieved successfully...";
        ApiResponse<List<User>> body = new ApiResponse<>(HttpStatus.OK, message, users);

        return ResponseEntity.ok(body);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse<User>> show(@PathVariable("id") int id) throws ResourceNotFoundException {

        User user = userService.getUserById(id);

        final String message = "The user retrieved successfully ..";
        ApiResponse<User> body = new ApiResponse<>(HttpStatus.OK, message, user);

        return ResponseEntity.ok(body);
    }

    @PostMapping("")
    public ResponseEntity<ApiResponse<User>> store(@Valid @RequestBody UserDto user, BindingResult result) {

        // Check validation errors
        if (result.hasErrors()) {
            String errors = result.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining("::"));

            ApiResponse<User> body = new ApiResponse<>(HttpStatus.BAD_REQUEST, errors, null);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
            // throw new ValidationException(errors);
        }

        final User createdUser = userService.createUser(user.mapToUser());
        log.info("{}", createdUser);

        final String message = "User created successfully..";
        ApiResponse<User> body = new ApiResponse<>(HttpStatus.CREATED, message, createdUser);

        return ResponseEntity.status(HttpStatus.CREATED).body(body);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ApiResponse<User>> update(@RequestBody UserDto user, BindingResult result,
            @PathVariable("id") int id) throws ResourceNotFoundException {
        if (result.hasErrors()) {
            String errors = result.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining("::"));
            throw new ValidationException(errors);
        }

        final User userToUpdate = userService.updateUser(user.mapToUser(), id);

        final String message = "User updated successfully..";
        ApiResponse<User> body = new ApiResponse<>(HttpStatus.OK, message, userToUpdate);

        return ResponseEntity.status(HttpStatus.CREATED).body(body);
    }

    @GetMapping("/{locationType}/{locationId}")
    public ResponseEntity<ApiResponse<List<User>>> getUserByLocation(@PathVariable("locationId") int locationId,
            @PathVariable("locationType") String locationType) throws ResourceNotFoundException {

        List<User> users = userService.getUsersByLocation(locationType, locationId);

        final String message = "The user retrieved successfully ..";
        ApiResponse<List<User>> body = new ApiResponse<>(HttpStatus.OK, message, users);

        return ResponseEntity.ok(body);
    }
}
