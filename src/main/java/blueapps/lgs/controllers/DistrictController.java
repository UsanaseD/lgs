package blueapps.lgs.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import blueapps.lgs.models.Request.DistrictRequest;
import blueapps.lgs.models.Response.DistrictResponse;
import blueapps.lgs.models.dto.DistrictDto;
import blueapps.lgs.services.DistrictService;
import blueapps.lgs.util.ApiResponse;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("api/v1/district")
@Log4j2
public class DistrictController {
    @Autowired
    private DistrictService districtService;

    @PostMapping
    public ResponseEntity<ApiResponse<DistrictResponse>> store(@Valid @RequestBody DistrictRequest districtRequest,
            BindingResult bindingResult) {
        // Check validation errors
        if (bindingResult.hasErrors()) {
            String errors = bindingResult.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining("::"));
            throw new ValidationException(errors);
        }
        

        DistrictDto districtDto = districtRequest.mapToDto();
        
        DistrictDto createdDistrict = districtService.createDistrict(districtDto);

        DistrictResponse district = createdDistrict.mapToResponse();

        final String message = "District created successfully...";
        ApiResponse<DistrictResponse> body = new ApiResponse<>(HttpStatus.OK, message,
        district);
        return ResponseEntity.status(HttpStatus.CREATED).body(body);
    }

    @GetMapping
    public ResponseEntity<ApiResponse<List<DistrictResponse>>> index() {
        List<DistrictResponse> districtResponses = new ArrayList<>();
        List<DistrictDto> districtDtos = districtService.getDistricts();
        log.info("{}", districtDtos);

        for(DistrictDto districtDto: districtDtos){
            DistrictResponse districtResponse = districtDto.mapToResponse();
            districtResponses.add(districtResponse);
        }

        final String message = "Districts retrieved successfully...";
        ApiResponse<List<DistrictResponse>> body = new ApiResponse<>(HttpStatus.OK, message,
        districtResponses);
        return ResponseEntity.ok(body);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse<DistrictResponse>> show(@PathVariable("id") Long id) {
        DistrictDto districtDto = districtService.getDistrict(id);
        log.info("{}", districtDto);
        
        DistrictResponse response = districtDto.mapToResponse();

        final String message = "District retrieved successfully...";
        ApiResponse<DistrictResponse> body = new ApiResponse<>(HttpStatus.OK, message,
        response);
        return ResponseEntity.ok(body);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ApiResponse<DistrictResponse>> update(@PathVariable Long id, @Valid @RequestBody DistrictRequest districtRequest,
    BindingResult bindingResult){     
        // Check validation errors
        if (bindingResult.hasErrors()) {
            String errors = bindingResult.getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining("::"));
            throw new ValidationException(errors);
        }
           
        
        DistrictDto districtDto = districtRequest.mapToDto();
        
        DistrictDto updatedDistrictDto = districtService.updateDistrict(districtDto, id);
        
        DistrictResponse response = updatedDistrictDto.mapToResponse();

        final String message = "District updated successfully...";
        ApiResponse<DistrictResponse> body = new ApiResponse<>(HttpStatus.OK, message, response
        );
        return ResponseEntity.status(HttpStatus.CREATED).body(body);
    }

    @GetMapping("/province/{keyword}")
    public ResponseEntity<ApiResponse<List<DistrictResponse>>> distPerProv(@PathVariable("keyword") String keyword) {
        List<DistrictResponse> districtResponses = new ArrayList<>();
        List<DistrictDto> districtDtos = districtService.getDistrictsByProvince(keyword);
        // log.info("{}", districtDtos);

        for(DistrictDto districtDto: districtDtos){
            DistrictResponse districtResponse = districtDto.mapToResponse();
            districtResponses.add(districtResponse);
        }

        final String message = "Districts from "+ keyword +" retrieved successfully...";
        ApiResponse<List<DistrictResponse>> body = new ApiResponse<>(HttpStatus.OK, message,
        districtResponses);
        return ResponseEntity.ok(body);
    }
}
