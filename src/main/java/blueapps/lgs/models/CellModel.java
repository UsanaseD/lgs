package blueapps.lgs.models;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.BeanUtils;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import blueapps.lgs.models.dto.CellDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Entity
@Table(name = "cell")
@Data
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Slf4j
public class CellModel {
        @TableGenerator(name = "cell", table = "ID_CELL", pkColumnName = "CELL_KEY", valueColumnName = "CELL_VALUE", pkColumnValue = "id", allocationSize = 1)
        @Id
        @GeneratedValue(strategy = GenerationType.TABLE, generator = "cell")
        private Long id;
        @CreationTimestamp
        private ZonedDateTime date;
        @UpdateTimestamp
        @JsonDeserialize(using = LocalDateTimeDeserializer.class)
        @JsonSerialize(using = LocalDateTimeSerializer.class)
        private LocalDateTime updateDateTime;
        private String name;
        private Long surface;
        @JsonIgnore
        @ToString.Exclude
        @OneToMany(mappedBy = "cell", cascade = CascadeType.ALL)
        private List<VillageModel> villages;
        @ManyToOne
        @JoinColumn(name = "Sector_Id")
        private SectorModel sector;

        public CellDto mapToDto() {
                CellDto cell = new CellDto();
                BeanUtils.copyProperties(this, cell);
                cell.setSector(this.sector.mapToDto());
                log.info("{}", cell);
                return cell;
        }

}
