package blueapps.lgs.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.BeanUtils;

import blueapps.lgs.models.dto.RecordDto;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames={"nid"})})
public class Record {
    
    @Id
    @GeneratedValue
    private Long id;
    private String firstName;
    private String lastName;
    @Column(unique=true)
    private String nid;
    private String contact;
    private String ubudehe;
    private String status = "active";
    private String description;
    @CreationTimestamp
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime createDateTime;
    @UpdateTimestamp
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime updateDateTime;
    private Boolean isDeleted = false;
    @ManyToOne
    @JoinColumn(name = "village_id")
    private VillageModel village;
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    public Record(String firstName, String lastName, String nid, String contact,String ubudehe, String status, String description, Boolean isDeleted) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nid = nid;
        this.contact = contact;
        this.ubudehe = ubudehe;
        this.status = status;
        this.description = description;
        this.isDeleted = isDeleted;
    }

    public RecordDto mapToDto(){
        RecordDto recordDto = new RecordDto();
        BeanUtils.copyProperties(this, recordDto);
        recordDto.setCategory(this.category.mapToDto());
        recordDto.setVillage(this.village.mapToDto());
        return recordDto;
    }
}
