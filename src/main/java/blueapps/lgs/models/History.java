package blueapps.lgs.models;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.beans.BeanUtils;

import blueapps.lgs.models.dto.HistoryDto;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
public class History {
    
    @Id
    @GeneratedValue
    private Integer id;
    private Integer recordId;
    private String recordName;
    private String recordNid;
    private String action;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @CreationTimestamp
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime createDateTime;

    public History(Integer recordId, String recordName, String recordNid, String action) {
        this.recordId = recordId;
        this.recordName = recordName;
        this.recordNid = recordNid;
        this.action = action;
    }

    public HistoryDto mapToDto(){
        HistoryDto history = new HistoryDto();
        BeanUtils.copyProperties(this, history);
        return history;
    }

}
