package blueapps.lgs.models.dto;

import java.time.LocalDateTime;

import org.springframework.beans.BeanUtils;

import blueapps.lgs.models.Category;
import blueapps.lgs.models.Response.HistoryResponse;
import lombok.Data;

@Data
public class HistoryDto {
    private Integer id;
    private Integer recordId;
    private String recordName;
    private String recordNid;
    private String action;
    private Category category;
    private LocalDateTime createDateTime;

    public HistoryResponse mapToResponse(){
        HistoryResponse historyResponse = new HistoryResponse();
        BeanUtils.copyProperties(this, historyResponse);
        return historyResponse;
    }
}
