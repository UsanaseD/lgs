package blueapps.lgs.models.dto;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import org.springframework.beans.BeanUtils;

import blueapps.lgs.models.SectorModel;
import blueapps.lgs.models.Response.DistrictResponse;
import blueapps.lgs.models.Response.SectorResponse;
import lombok.Data;

@Data
public class SectorDto {
    private Long id;
    private ZonedDateTime date;
    private LocalDateTime updateDateTime;
    private String name;
    private Long surface;
    private DistrictDto district;

    
    public SectorResponse mapToResponse(){
        SectorResponse sectorResponse = new SectorResponse();
        DistrictResponse districtResponse = this.district.mapToResponse();
        BeanUtils.copyProperties(this, sectorResponse);
        sectorResponse.setDistrict(districtResponse);
        return sectorResponse;
    }

    public SectorModel mapToEntity(){
        SectorModel sectorModel = new SectorModel();
        BeanUtils.copyProperties(this, sectorModel);

        return sectorModel;
    }
}
