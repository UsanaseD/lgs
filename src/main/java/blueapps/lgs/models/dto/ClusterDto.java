package blueapps.lgs.models.dto;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.beans.BeanUtils;

import blueapps.lgs.models.Category;
import blueapps.lgs.models.ClusterModel;
import blueapps.lgs.models.Response.ClusterResponse;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class ClusterDto {
    private Integer id;
    private ZonedDateTime date;
    private LocalDateTime updateDateTime;
    private String name;
    private String email;
    private String phone;
    private String website;
    private String description;
    private String status;
    private Boolean deleted;
    private List<Category> categories;
    // private Integer numberOfCategories;
    // private Integer records;


    public ClusterResponse mapToResponse() {
        ClusterResponse clusterResponse = new ClusterResponse();
        BeanUtils.copyProperties(this, clusterResponse);
        // clusterResponse.setNumberOfCategories(categories.size());
        return clusterResponse;
    }

    public ClusterModel mapToEntity(){
        ClusterModel clusterModel = new ClusterModel();
        BeanUtils.copyProperties(this, clusterModel);
        return clusterModel;
    }
}
