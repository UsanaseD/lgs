package blueapps.lgs.models.dto;


import java.time.LocalDateTime;

import org.springframework.beans.BeanUtils;

import blueapps.lgs.models.Category;
import blueapps.lgs.models.Response.CategoryResponse;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class CategoryDto {
    private Integer id;
    private String name;
    private String description;
    private String status;
    private Boolean deleted;
    private ClusterDto cluster;
    //private Integer records;
    private LocalDateTime createDateTime;
    private LocalDateTime updateDateTime;

    public Category mapToEntity(){
        Category category = new Category();
        BeanUtils.copyProperties(this, category);
        // category.setCluster(cluster.mapToEntity());
        return category;
    }

    public CategoryResponse mapToResponse(){
        CategoryResponse categoryResponse = new CategoryResponse();
        BeanUtils.copyProperties(this, categoryResponse);
        categoryResponse.setCluster(this.cluster.mapToResponse());
        return categoryResponse;
    }
}
