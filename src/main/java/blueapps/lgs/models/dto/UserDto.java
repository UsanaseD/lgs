package blueapps.lgs.models.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import blueapps.lgs.models.User;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserDto {
    private String firstName;
    @NotBlank(message = "Please provide your first name")
    @Size(min=3, message="Last name should be at least 3 character")
    private String lastName;
    @NotBlank(message = "Please provide your username")
    @NotNull
    private String userName;
    @NotBlank(message = "Please provide your email")
    @Email(message = "Invalid email")
    @NotNull
    private String email;
    @NotBlank(message = "Please provide your password")
    @NotNull
    @Size(min=8, message = "Password should be at least 8 characters")
    private String password;
    @NotBlank(message = "Please provide your locationType")
    private String locationType;
    @NotNull(message = "Please provide your locationId")
    private int locationId;
    private String roles;
    private Boolean active = true;

    public User mapToUser() {
        return new User(firstName, lastName, userName, email, password, User.ELOCATIONTYPE.valueOf(locationType), locationId, roles, true);
      }

}
