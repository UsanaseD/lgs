package blueapps.lgs.models.dto;

import org.springframework.beans.BeanUtils;

import blueapps.lgs.models.Record;
import blueapps.lgs.models.Response.RecordResponse;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class RecordDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String nid;
    private String contact;
    private String ubudehe;
    private String status;
    private String description;
    private VillageDto village;
    private CategoryDto category;

    public Record mapToRecord() {
        Record record = new Record();
        BeanUtils.copyProperties(this, record);
        // record.setCategory(category.mapToEntity());
        // record.setVillage(village.mapToEntity());
        return record;
    }

    public RecordResponse mapToResponse(){
        RecordResponse recordResponse = new RecordResponse();
        BeanUtils.copyProperties(this, recordResponse);
        recordResponse.setCategory(this.category.mapToResponse());
        recordResponse.setVillage(this.village.mapToResponse());
        return recordResponse;
    }
}
