package blueapps.lgs.models.dto;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import org.springframework.beans.BeanUtils;

import blueapps.lgs.models.VillageModel;
import blueapps.lgs.models.Response.VillageResponse;
import lombok.Data;

@Data
public class VillageDto {
    private Long id;
    private ZonedDateTime date;
    private LocalDateTime updateDateTime;
    private String name;
    private Long amasibo;
    private Long surface;
    private CellDto cell;

    public VillageModel mapToEntity(){
        VillageModel village = new VillageModel();
        BeanUtils.copyProperties(this, village);
        return village;
    }
    
    public VillageResponse mapToResponse(){
        VillageResponse village = new VillageResponse();
        BeanUtils.copyProperties(this, village);
        village.setCell(this.cell.mapToResponse());
        return village;
    }
}
