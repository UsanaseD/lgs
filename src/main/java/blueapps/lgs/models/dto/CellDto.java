package blueapps.lgs.models.dto;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import org.springframework.beans.BeanUtils;

import blueapps.lgs.models.CellModel;
import blueapps.lgs.models.Response.CellResponse;
import lombok.Data;

@Data
public class CellDto {
    private Long id;
    private ZonedDateTime date;
    private LocalDateTime updateDateTime;
    private String name;
    private Long surface;
    private SectorDto sector;

    public CellModel mapToEntity(){
        CellModel cell = new CellModel();
        BeanUtils.copyProperties(this, cell);
        return cell;
    }

    public CellResponse mapToResponse(){
        CellResponse cell = new CellResponse();
        BeanUtils.copyProperties(this, cell);
        cell.setSector(this.sector.mapToResponse());
        return cell;
    }
    
}


