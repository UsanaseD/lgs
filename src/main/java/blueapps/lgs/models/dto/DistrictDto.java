package blueapps.lgs.models.dto;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import org.springframework.beans.BeanUtils;

import blueapps.lgs.models.DistrictModel;
import blueapps.lgs.models.Response.DistrictResponse;
import lombok.Data;

@Data
public class DistrictDto {
    private Long id;
    private ZonedDateTime date;
    private LocalDateTime updateDateTime;
    private String name;
    private Long surface;
    private String province;

    public DistrictResponse mapToResponse(){
        DistrictResponse district = new DistrictResponse();
        BeanUtils.copyProperties(this, district);

        return district;
    }

    public DistrictModel mapToEntity(){
        DistrictModel district = new DistrictModel();
        BeanUtils.copyProperties(this, district);
        return district;
    }
}
