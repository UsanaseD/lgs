package blueapps.lgs.models.Request;

import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.beans.BeanUtils;

import blueapps.lgs.models.dto.CategoryDto;
import lombok.Data;

@Data
public class CategoryRequest {
    @NotBlank(message="Category name can't be blank")
    private String name;
    @NotBlank(message="Description can't be blank")
    private String description;
    @NotNull(message="Cluster ID can't be blank")
    private Integer clusterId;

    

    
    public CategoryDto mapToDto(){
        CategoryDto categoryDto = new CategoryDto();
        BeanUtils.copyProperties(this, categoryDto);
        return categoryDto;
    }
    
}
