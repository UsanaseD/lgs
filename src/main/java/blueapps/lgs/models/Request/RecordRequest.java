package blueapps.lgs.models.Request;

import org.springframework.beans.BeanUtils;

import blueapps.lgs.models.dto.RecordDto;
import lombok.Data;

@Data
public class RecordRequest {
    private String firstName;
    private String lastName;
    private String nid;
    private String contact;
    private String ubudehe;
    private String description;
    private String status;
    private Long villageId;
    private Long categoryId;

    public RecordDto mapToDto(){
        RecordDto recordDto = new RecordDto();
        BeanUtils.copyProperties(this, recordDto);
        return recordDto;
    }
}
