package blueapps.lgs.models.Request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.beans.BeanUtils;

import blueapps.lgs.models.dto.SectorDto;
import lombok.Data;

@Data
public class SectorRequest {
    @NotBlank(message = "Name can't be blank")
    private String name;
    @NotNull(message = "Surface can't be blank")
    private Long surface;
    @NotNull(message = "District ID can't be blank")
    private Long districtId;

    public SectorDto mapToDto(){
        SectorDto sectorDto = new SectorDto();
        BeanUtils.copyProperties(this, sectorDto);

        return sectorDto;
    }
}
