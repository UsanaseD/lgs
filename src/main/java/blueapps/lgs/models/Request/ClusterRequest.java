package blueapps.lgs.models.Request;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.beans.BeanUtils;

import blueapps.lgs.models.dto.ClusterDto;
import lombok.Data;

@Data
public class ClusterRequest {
    @NotBlank(message = "Name can't be blank")
    private String name;
    @NotBlank(message = "Email can't be blank")
    @Email
    @Column(name = "email")
    private String email;
    @NotBlank(message = "phone can't be blank")
    @Size(min = 10, max = 10, message = "phone number has to be 10 characters")
    @Column(name = "phone")
    private String phone;
    private String website;
    @Size(min = 5, max = 100, message = "Description can't be null")
    private String description;

    public ClusterDto mapToDto(){
        ClusterDto clusterDto = new ClusterDto();
        BeanUtils.copyProperties(this, clusterDto);
        return clusterDto;
    }
}
