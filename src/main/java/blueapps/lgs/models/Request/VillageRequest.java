package blueapps.lgs.models.Request;

import javax.validation.constraints.NotNull;

import org.springframework.beans.BeanUtils;

import blueapps.lgs.models.dto.VillageDto;
import lombok.Data;

@Data
public class VillageRequest {
    private String name;
    @NotNull(message = "how many are Masibo in this cell?")
    private Long amasibo;
    @NotNull(message = "what's this Village's surface?")
    private Long surface;
    @NotNull(message = "Cell ID can't be empty")
    private Long cellId;

    public VillageDto mapToDto(){
        VillageDto villageDto = new VillageDto();
        BeanUtils.copyProperties(this, villageDto);
        return villageDto;
    }
}
