package blueapps.lgs.models.Request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.beans.BeanUtils;

import blueapps.lgs.models.dto.DistrictDto;
import lombok.Data;

@Data
public class DistrictRequest {
    @NotBlank(message="District name can't be blank")
    private String name;
    @NotNull(message="Surface can't be blank")
    private Long surface;
    @NotBlank(message="Province name can't be blank")
    private String province;

    public DistrictDto mapToDto(){
        DistrictDto district = new DistrictDto();
        BeanUtils.copyProperties(this, district);

        return district;
    }
}
