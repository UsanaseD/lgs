package blueapps.lgs.models.Request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.beans.BeanUtils;

import blueapps.lgs.models.dto.CellDto;
import lombok.Data;

@Data
public class CellRequest {
    @NotBlank(message="Cell name can't be blank")
    private String name;
    @NotNull(message="Surface name can't be blank")
    private Long surface;
    @NotNull(message="Sector ID can't be blank")
    private Long sectorId;

    public CellDto mapToDto(){
        CellDto cellDto = new CellDto();
        BeanUtils.copyProperties(this, cellDto);

        return cellDto;
    }
}
