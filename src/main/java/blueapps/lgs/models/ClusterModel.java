package blueapps.lgs.models;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.AssertFalse;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.BeanUtils;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import blueapps.lgs.models.Response.ClusterResponse;
import blueapps.lgs.models.dto.ClusterDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "cluster")
@Data
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class ClusterModel {
        @TableGenerator(name = "cluster", table = "ID_CLUST", pkColumnName = "CLUST_KEY", valueColumnName = "CLUST_VALUE", pkColumnValue = "id", allocationSize = 1)
        @Id
        @GeneratedValue(strategy = GenerationType.TABLE, generator = "cluster")
        private Integer id;
        @CreationTimestamp
        private ZonedDateTime date;
        @UpdateTimestamp
        @JsonDeserialize(using = LocalDateTimeDeserializer.class)
        @JsonSerialize(using = LocalDateTimeSerializer.class)
        private LocalDateTime updateDateTime;
        private String name;
        private String email;
        private String phone;
        private String website;
        private String description;
        private String status = "active";
        @AssertFalse
        private Boolean deleted = false;
        @JsonIgnore
        @ToString.Exclude
        @OneToMany(mappedBy = "cluster", cascade = CascadeType.ALL)
        private List<Category> categories;

        public ClusterModel(String name, String email, String phone, String website, String description, String status,
                        @AssertFalse Boolean deleted, ZonedDateTime date, LocalDateTime updateDateTime) {
                this.name = name;
                this.email = email;
                this.phone = phone;
                this.website = website;
                this.description = description;
                this.status = status;
                this.deleted = deleted;
                this.date = date;
                this.updateDateTime = updateDateTime;
        }

        public ClusterResponse mapToResponse() {
                ClusterResponse clusterResponse = new ClusterResponse();
                BeanUtils.copyProperties(this, clusterResponse);
                return clusterResponse;
        }

        public ClusterDto mapToDto() {
                ClusterDto clusterDto = new ClusterDto();
                BeanUtils.copyProperties(this, clusterDto);
                return clusterDto;
        }
}
