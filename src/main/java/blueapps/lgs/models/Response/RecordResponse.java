package blueapps.lgs.models.Response;

import lombok.Data;

@Data
public class RecordResponse {
    private Long id;
    private String firstName;
    private String lastName;
    private String nid;
    private String contact;
    private String ubudehe;
    private String status;
    private String description;
    private VillageResponse village;
    private CategoryResponse category;
}
