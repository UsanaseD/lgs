package blueapps.lgs.models.Response;

import lombok.Data;

@Data
public class DistrictResponse {
    private Long id;
    private String name;
    private Long surface;
    private String province;    
}
