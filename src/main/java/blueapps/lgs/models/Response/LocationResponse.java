package blueapps.lgs.models.Response;

import lombok.Data;

@Data
public class LocationResponse {
   private Long locationId; 
   private String locationType; 
   private Object locationObject; 
}
