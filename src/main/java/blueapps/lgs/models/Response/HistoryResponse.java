package blueapps.lgs.models.Response;

import java.time.LocalDateTime;

import blueapps.lgs.models.Category;
import lombok.Data;

@Data
public class HistoryResponse {
    private Integer id;
    private Integer recordId;
    private String recordName;
    private String recordNid;
    private String action;
    private Category category;
    private LocalDateTime createDateTime;
}
