package blueapps.lgs.models.Response;

import lombok.Data;

/**
 * CellResponse
 */
@Data
public class CellResponse {    
    private Long id;
    private String name;
    private Long surface;
    private SectorResponse sector;
}