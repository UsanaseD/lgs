package blueapps.lgs.models.Response;

import lombok.Data;

@Data
public class VillageResponse {
    private Long id;
    private String name;
    private Long amasibo;
    private Long surface;
    private CellResponse cell;
}
