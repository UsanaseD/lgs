package blueapps.lgs.models.Response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ReportResponse {
    private String name;
    private Integer size;
}
