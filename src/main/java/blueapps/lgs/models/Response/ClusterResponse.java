package blueapps.lgs.models.Response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ClusterResponse {
    private Integer id;
    private String name;
    private String email;
    private String phone;
    private String website;
    private String description;
    private String status;
    private Boolean deleted;
    private Integer numberOfCategories;
    private Integer records;

    public ClusterResponse(Integer id, String name, String email, String phone, String website, String description,
            String status, Boolean deleted) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.website = website;
        this.description = description;
        this.status = status;
        this.deleted = deleted;
    }

}
