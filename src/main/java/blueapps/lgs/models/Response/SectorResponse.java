package blueapps.lgs.models.Response;

import lombok.Data;

@Data
public class SectorResponse {
    private Long id;
    private String name;
    private Long surface;
    private DistrictResponse district;
}
