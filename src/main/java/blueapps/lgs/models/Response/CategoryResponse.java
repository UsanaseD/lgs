package blueapps.lgs.models.Response;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class CategoryResponse {
    private Integer id;
    private String name;
    private String description;
    private Boolean deleted;
    private String status;
    private ClusterResponse cluster;
    private Integer records;
    private LocalDateTime createDateTime;
    private LocalDateTime updateDateTime;

    public CategoryResponse(Integer id, String name, String description,
                            Boolean deleted,
                            String status,
                            LocalDateTime createDateTime,
                            LocalDateTime updateDateTime) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.deleted = deleted;
        this.status = status;
        this.createDateTime = createDateTime;
        this.updateDateTime = updateDateTime;
    }

}
