package blueapps.lgs.models;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.BeanUtils;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import blueapps.lgs.models.dto.VillageDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "village")
@Data
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class VillageModel {
    @TableGenerator(
            name="village",
            table="ID_VILL",
            pkColumnName="VILL_KEY",
            valueColumnName="VILL_VALUE",
            pkColumnValue="id",
            allocationSize=1)
    @Id
    @GeneratedValue(
            strategy= GenerationType.TABLE,
            generator="village"
    )
    private Long id;
//    @GeneratedValue(generator = "uuid2")
//    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
//    @Column(name = "id", columnDefinition = "VARCHAR(255)")
//    private UUID id;
    @CreationTimestamp
    private ZonedDateTime date;
    @UpdateTimestamp
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime updateDateTime;
    private String name;
    private Long amasibo;
    private Long surface;
    @ManyToOne
    @JoinColumn(name = "Cell_Id")
    private CellModel cell;
    @JsonIgnore
    @ToString.Exclude
    @OneToMany(mappedBy = "village", cascade = CascadeType.ALL)
    private List<Record> records;

    public VillageDto mapToDto(){
        VillageDto village = new VillageDto();
        BeanUtils.copyProperties(this, village);
        village.setCell(this.cell.mapToDto());
        return village;
    }
}
