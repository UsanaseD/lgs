package blueapps.lgs.models;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.BeanUtils;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import blueapps.lgs.models.dto.DistrictDto;
import lombok.Data;
import lombok.ToString;

@Entity
@Table(name = "district")
@Data
@EntityListeners(AuditingEntityListener.class)
public class DistrictModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    private Long id;
    @CreationTimestamp
    private ZonedDateTime date;
    @UpdateTimestamp
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime updateDateTime;
    @Column(unique = true)
    private String name;
    private Long surface;
    private String province;
    @JsonIgnore
    @ToString.Exclude
    @OneToMany(mappedBy = "district", cascade = CascadeType.ALL)
    private List<SectorModel> sectors;

    public DistrictDto mapToDto() {
        DistrictDto districtDto = new DistrictDto();
        BeanUtils.copyProperties(this, districtDto);
        return districtDto;
    }


}
