package blueapps.lgs.models;

import java.io.Serializable;

import lombok.Data;

@Data
public class AuthenticationRequest implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3852565455217078182L;
    /**
     *
     */

    private String email;
    private String password;
}