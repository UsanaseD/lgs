package blueapps.lgs.models;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import blueapps.lgs.models.Response.CategoryResponse;
import blueapps.lgs.models.dto.CategoryDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Data
@NoArgsConstructor
@Entity
@Slf4j
public class Category {
    
    @Id
    @GeneratedValue
    private Integer id;
    @Column(unique = true)
    private String name;
    private String description;
    private Boolean deleted ;
    private String status ;
    @CreationTimestamp
    @DateTimeFormat()
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MMMM-yyyy HH:MM")
    private LocalDateTime createDateTime;
    @UpdateTimestamp
    @DateTimeFormat()
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MMMM-yyyy HH:MM")
    private LocalDateTime updateDateTime;
    @ManyToOne
    @JoinColumn(name = "cluster_id")
    private ClusterModel cluster;
    @JsonIgnore
    @ToString.Exclude
    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
    private List<Record> records;

    public Category(String name, String description, Boolean deleted,
                    String status, LocalDateTime createDateTime,LocalDateTime updateDateTime) {
        this.name = name;
        this.description = description;
        this.deleted = deleted;
        this.status = status;
        this.createDateTime = createDateTime;
        this.updateDateTime = updateDateTime;
    }

    public CategoryResponse mapToCategoryResponse(){
        return new CategoryResponse(id, name, description, deleted, status,createDateTime,updateDateTime);
    }

    public CategoryDto mapToDto(){
        CategoryDto categoryDto = new CategoryDto();
        BeanUtils.copyProperties(this, categoryDto);
        categoryDto.setCluster(this.cluster.mapToDto());
        log.info("{}", categoryDto);
        return categoryDto;
    }

}
