package blueapps.lgs.models;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.BeanUtils;

import blueapps.lgs.models.dto.DistrictDto;
import blueapps.lgs.models.dto.SectorDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@NoArgsConstructor
@Table(name = "sector")
@EntityListeners(EntityListeners.class)
public class SectorModel {
        @TableGenerator(name = "sector", table = "ID_GEN", pkColumnName = "GEN_KEY", valueColumnName = "GEN_VALUE", pkColumnValue = "id", allocationSize = 1)
        @Id
        @GeneratedValue(strategy = GenerationType.TABLE, generator = "sector")
        private Long id;
        @CreationTimestamp
        private ZonedDateTime date;
        @UpdateTimestamp
        @JsonDeserialize(using = LocalDateTimeDeserializer.class)
        @JsonSerialize(using = LocalDateTimeSerializer.class)
        private LocalDateTime updateDateTime;
        private String name;
        private Long surface;
        @ManyToOne
        @JoinColumn(name = "DISTRICT_ID")
        private DistrictModel district;
        @JsonIgnore
        @ToString.Exclude
        @OneToMany(mappedBy = "sector", cascade = CascadeType.ALL)
        private List<CellModel> cells;

        public SectorModel(String name, Long surface) {
                this.name = name;
                this.surface = surface;
        }

        public SectorDto mapToDto() {
                SectorDto sectorDto = new SectorDto();
                DistrictDto districtDto = this.district.mapToDto();
                BeanUtils.copyProperties(this, sectorDto);
                sectorDto.setDistrict(districtDto);
                return sectorDto;
        }
}
