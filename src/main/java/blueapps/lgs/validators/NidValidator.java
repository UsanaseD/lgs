package blueapps.lgs.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import blueapps.lgs.validators.constraints.NidConstraint;

public class NidValidator implements 
  ConstraintValidator<NidConstraint, String> {

    @Override
    public void initialize(NidConstraint nid) {
    }

    @Override
    public boolean isValid(String nidField,
      ConstraintValidatorContext cxt) {
        return nidField != null && nidField.matches("^[1-3](19|20)\\d{2}[7-8]\\d{7}[0-9]\\d{2}$");
    }

}